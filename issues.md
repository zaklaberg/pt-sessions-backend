#issues
- add getbyid/getall to clientservice
- add KG to weight in ExerciseSet
- session should also have a trainer? and a given trainer should only see his assigned sessions?
- allow POST to program with a list of workout ids
- services.AddRouting(options => options.LowercaseUrls = true); ( put before addmvc(), addmvc() then skips addrouting() )
- programservice::addworkout: allow two behaviours: if id = 0, add the whole workout as new. if id != 0, use an existing workout, neglect other fields
- trainer is poco already, there is no need for a service / dto. but for extensibility, we may want to add these anyway.
- fix all created(ataction) to use nameof
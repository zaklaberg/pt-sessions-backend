# PT Sessions
PT Sessions is an application that is responsible for handling the sessions trainers book with clients. These sessions include exercises that the trainer has decided the client should be doing. These sessions can be performed at many venues, including: gyms, homes, and public areas. This application is something that a trainer takes with them on the job and is used to track and manage their client’s progress.

## API
The current project is the backend / API for the `PT Sessions` application. Except registration and login it contains no public endpoints, and the intended user base includes only authorized trainers or system administrators. Administrators can do everything a trainer can, and may additionally access an endpoint to activate/deactivate trainers.

## Team members and project participants
#### Team members
- Ole Hansson
- Birger Topphol
- Zakarias Laberg
- Camilla Arntzen


## Installation
- Clone
- Add a connection string to a database, and make sure `Startup.cs` uses the string you add. Contact a project maintainer to get access to the official database
- If using a fresh database, run `add-migration idb` and then `update-database` in the package manager console. Some data is seeded by default - you can comment out the call to `ModelBuilder::Seed` in `PtSessionsDbContext::OnModelCreating` if you do not want seeding.
- API endpoints require authorization; if you're running the API yourself it's easiest to just comment out the `[Authorize]` attributes present at the top of the controller classes.
- Run!

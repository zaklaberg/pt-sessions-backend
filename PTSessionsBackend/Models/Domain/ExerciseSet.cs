﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    public class ExerciseSet
    {
        [Required]
        public int Id { get; set; }

        // Repetitions to perform for this set
        [Required]
        public int NumberOfReps { get; set; }

        // Weight to be used, if applicable
        public int WeightKG { get; set; }

        // Time to rest after each rep, if applicable
        public int RestPeriodSeconds { get; set; }

        // Tempo describes [number of seconds down] - [number of seconds held at bottom] - [number of seconds up] - [number of seconds held at top]
        // E.g. for a pushup, a tempo might be: 2-0-2-0 
        public string Tempo { get; set; }

        // Owned by a WorkoutExercise
        public int WorkoutExerciseId { get; set; }
        public WorkoutExercise WorkoutExercise { get; set; }
    }
}
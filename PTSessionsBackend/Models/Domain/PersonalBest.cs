﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class PersonalBest
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public int ExerciseId { get; set; } 
        public Exercise Exercise { get; set; }

        public int Value { get; set; }
        public string Unit { get; set; }
        public DateTime Achieved { get; set; }
    }
}

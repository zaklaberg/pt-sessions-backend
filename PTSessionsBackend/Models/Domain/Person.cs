﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
         
        public string Email { get; set; }

        public string Address { get; set; }
        
        public DateTime DOB { get; set; }


        // FKs
        public int? TrainerId { get; set; }
        public Trainer Trainer { get; set; }

        public int? AdminId { get; set; }
        public Admin Admin { get; set; }

        public int? ClientId { get; set; }
        public Client Client { get; set; }
    }
}

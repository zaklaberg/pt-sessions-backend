﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    public class Program
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; } 
        
        // A program belongs to one specific client
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public ICollection<Session> Sessions { get; set; }

        public ICollection<Workout> Workouts { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class ExerciseType
    {
        [Required]
        public int Id { get; set; }

        [Required, MinLength(2)]
        public string Name { get; set; }

        public ICollection<Exercise> Exercises { get; set; } 
    }
}

﻿using System;

namespace PTSessionsBackend.Models.Domain
{
    public class Session
    {
        public int Id { get; set; } = 0;
        public DateTime Time { get; set; }

        public int ProgramId { get; set; }
        public Program Program { get; set; }

        public bool Completed { get; set; } = false; 

        public int VenueId { get; set; }
        public Venue Venue { get; set; }
        
        public Workout Workout { get; set; }
    }
}

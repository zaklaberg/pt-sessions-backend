﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    public class Exercise
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(30, MinimumLength = 2)]
        public string Name { get; set; }

        public string Description { get; set; }

        // Exercises will belong to multiple WorkoutExercises
        public ICollection<WorkoutExercise> WorkoutExercises { get; set; }
         
        // Many-to-many
        // Category describes the body area(s) the exercise targets. E.g. shoulders, biceps and so on.
        public ICollection<ExerciseCategory> Categories { get; set; }

        // Type describes what the exercise is used for. E.g. used to build strength, endurance, balance and so on.
        public ICollection<ExerciseType> Types { get; set; }
        
        public ICollection<PersonalBest> PersonalBests { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class Venue
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
         
        public int TypeId { get; set; }
        public VenueType Type { get; set; }
        
        public ICollection<Session> Sessions { get; set; }
    }
}

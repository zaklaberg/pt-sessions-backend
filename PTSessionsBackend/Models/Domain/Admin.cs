﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class Admin
    {
        public int Id { get; set; }

        public Person Person { get; set; }
    }
}
 
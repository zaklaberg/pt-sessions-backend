﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    // This type of workout is intended for use in a Program 
    // Suggests that we add base workouts to base custom workouts off.
    public class Workout
    {
        [Required]
        public int Id { get; set; }

        [Required, MinLength(2)]
        public string Name { get; set; }
        public string Description { get; set; }

        public int? SessionId { get; set; } 
        public Session Session { get; set; }

        public int? ProgramId { get; set; }
        public Program Program { get; set; }
        
        [Range(1, 7)]
        public int Day { get; set; }

        [Range(1, 666)]
        public int Week { get; set; }

        public ICollection<WorkoutExercise> WorkoutExercises { get; set; }
    }
}
﻿namespace PTSessionsBackend.Models.Domain
{
    public class Trainer
    {
        public int Id { get; set; }
        
        public Person Person { get; set; }
        public string ProfilePicture { get; set; }

        public bool Active { get; set; } = false;
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.Domain
{
    public class VenueType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int VenueId { get; set; }
        public ICollection<Venue> Venues { get; set; }
    }
}
 
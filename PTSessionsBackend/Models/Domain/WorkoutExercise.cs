﻿using System.Collections.Generic;

namespace PTSessionsBackend.Models.Domain
{
    // Describes an exercise in a workout, and its sets
    public class WorkoutExercise
    {
        public int Id { get; set; }

        // Owned by a single workout, which will typically be owned by a program/client
        // This is so these can be individually edited
        public int WorkoutId { get; set; }
        public Workout Workout { get; set; }

        // Describe static info about an exercise
        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }

        // Sets contain info about reps, weights, rest and so on. 
        public ICollection<ExerciseSet> Sets { get; set; }
    }
}

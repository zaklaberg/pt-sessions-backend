﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    public class ExerciseCategory
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Exercise> Exercises { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.Domain
{
    public class Client
    {
        public int Id { get; set; }

        public Person Person { get; set; }

        public string PhoneNumber { get; set; }
        
        public float HeightCM { get; set; } 
        public float WeightKG { get; set; }

        public ICollection<PersonalBest> PersonalBests { get; set; }

        // Client may have multiple programs
        public ICollection<Program> Programs { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models.Domain;

namespace PTSessionsBackend.Models
{
    /// <summary>
    ///  Extension method on the ModelBuilder for seeding data
    /// </summary>
    public static class ModelBuilderExtensions
    {
        private static void SeedVenueTypes(ModelBuilder builder)
        {
            builder.Entity<VenueType>().HasData(
                new VenueType
                {
                    Id = 1,
                    Name = "Home"
                },
                new VenueType
                {
                    Id = 2,
                    Name = "Gym",
                },
                new VenueType
                {
                    Id = 3,
                    Name = "Public"
                }
            );
        }

        private static void SeedVenues(ModelBuilder builder)
        {
            builder.Entity<Venue>().HasData(
                new Venue
                {
                    Id = 1,
                    Name = "Gymmicus",
                    Description = "The local gym",
                    Address = "Slottsgata 1 0328 SLOTTET",
                    TypeId = 2
                },
                new Venue
                {
                    Id = 2,
                    Name = "Casa Laberg",
                    Description = "The home of Client Numero Uno",
                    Address = "Fearnleys Gate 5A",
                    TypeId = 1
                },
                new Venue
                {
                    Id = 3,
                    Name = "Galdhøpiggen",
                    Description = "Top of a mountain",
                    Address = "Galdesand",
                    TypeId = 3
                }
            );
        }
        private static void SeedExercises(ModelBuilder builder)
        {
            builder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 1,
                    Name = "Push up",
                    Description = "Lift yourself with your own bloody arms.",
                },
                new Exercise
                {
                    Id = 2,
                    Name = "Pull up",
                    Description = "Pull yourself up with your own bloody arms.",
                }
            );
        }

        private static void SeedExerciseTypes(ModelBuilder builder)
        {
            builder.Entity<ExerciseType>().HasData(
                new ExerciseType
                {
                    Id = 1,
                    Name = "Endurance"
                },
                new ExerciseType
                {
                    Id = 2,
                    Name = "Strength"
                },
                new ExerciseType
                {
                    Id = 3,
                    Name = "Flexibility"
                },
                new ExerciseType
                {
                    Id = 4,
                    Name = "Balance"
                }
            );
        }

        private static void SeedExerciseCategories(ModelBuilder builder)
        {
            builder.Entity<ExerciseCategory>().HasData(
                    new ExerciseCategory
                    {
                        Id = 1,
                        Name = "Chest"
                    },
                    new ExerciseCategory
                    {
                        Id = 2,
                        Name = "Triceps"
                    },
                    new ExerciseCategory
                    {
                        Id = 3,
                        Name = "Biceps"
                    },
                    new ExerciseCategory
                    {
                        Id = 4,
                        Name = "Shoulders"
                    }
            );
        }

        private static void SeedExerciseCategoryJoinTable(ModelBuilder builder)
        {
            builder.Entity<Exercise>().HasMany(e => e.Categories).WithMany(c => c.Exercises).UsingEntity(j => j.HasData(
                new
                {
                    ExercisesId = 1,
                    CategoriesId = 1
                },
                new
                {
                    ExercisesId = 1,
                    CategoriesId = 2
                },
                new
                {
                    ExercisesId = 2,
                    CategoriesId = 2
                },
                new
                {
                    ExercisesId = 1,
                    CategoriesId = 3
                }
            ));
        }

        private static void SeedExerciseTypeJoinTable(ModelBuilder builder)
        {
            builder.Entity<Exercise>().HasMany(e => e.Types).WithMany(t => t.Exercises).UsingEntity(j => j.HasData(
                new
                {
                    ExercisesId = 1,
                    TypesId = 1
                },
                new
                {
                    ExercisesId = 2,
                    TypesId = 2
                },
                new
                {
                    ExercisesId = 2,
                    TypesId = 3
                }
            ));
        }
        
        private static void SeedPersons(ModelBuilder builder)
        {
            builder.Entity<Person>().HasData(
                new Person
                {
                    Id = 1,
                    TrainerId = 1,
                    FirstName = "Zak",
                    MiddleName = "\"The Trainer\"",
                    LastName = "Laberg",
                    Address = "Fearnleys Gate 5A",
                    Email = "zak.laberg@gmail.com",
                    DOB = System.DateTime.Now
                },
                new Person
                {
                    Id = 2,
                    ClientId = 1,
                    FirstName = "Dwayne",
                    LastName = "Ze Rock",
                    Address = "Rocktown",
                    Email = "the.rock@money.com",
                    DOB = System.DateTime.Now
                },
                new Person
                {
                    Id = 3,
                    AdminId = 1,
                    FirstName = "Zak",
                    MiddleName = "\"The Admin\"",
                    LastName = "Laberg",
                    Address = "somewhere in scandinavia",
                    Email = "zakkkkkk.laberg@gmail.com",
                    DOB = System.DateTime.Now
                }
            );
        }
        public static void SeedClients(ModelBuilder builder)
        {
            builder.Entity<Client>().HasData(
                new Client
                {
                    Id = 1,
                    PhoneNumber = "+42 90238791",
                    HeightCM = 192,
                    WeightKG = 90
                }
            );
        }

        public static void SeedTrainers(ModelBuilder builder)
        {
            builder.Entity<Trainer>().HasData(
                new Trainer
                {
                    Id = 1,
                    ProfilePicture = "some-url-to-profile-pic.jpg",
                    Active = false
                }
            );
        }

        public static void SeedAdmins(ModelBuilder builder)
        {
            builder.Entity<Admin>().HasData(
                new Admin
                {
                    Id = 1
                }
            );
        }

        public static void SeedPrograms(ModelBuilder builder)
        {
            builder.Entity<Program>().HasData(
                new Program
                {
                    Id = 1,
                    Name = "The Massive Muscle Program",
                    Description = "The aim of this program is to develop enormous muscles, primarily through consuming metric shittons of anabolic steroids.",
                    ClientId = 1,
                    Start = System.DateTime.Now,
                    End = System.DateTime.Now.AddMonths(2)
                }
            );
        }

        public static void SeedWorkouts(ModelBuilder builder)
        {
            builder.Entity<Workout>().HasData(
                new Workout
                {
                    Id = 1,
                    Name = "Ultra Arm Workout eXtreme",
                    Description = "In this workout we eat fifteen steroid pills a day for a month",
                    ProgramId = 1,
                    SessionId = 1
                }
            );
        }
        private static void SeedWorkoutExercises(ModelBuilder builder)
        {
            builder.Entity<WorkoutExercise>().HasData(
                new WorkoutExercise
                {
                    Id = 1,
                    WorkoutId = 1,
                    ExerciseId = 1,
                }
            );
        }

        private static void SeedExerciseSets(ModelBuilder builder)
        {
            builder.Entity<ExerciseSet>().HasData(
                new ExerciseSet
                {
                    Id = 1,
                    NumberOfReps = 55,
                    WeightKG = 10,
                    RestPeriodSeconds = 20,
                    Tempo = "2-2-2-2",
                    WorkoutExerciseId = 1
                }
            );
        }

        private static void SeedSessions(ModelBuilder builder)
        {
            builder.Entity<Session>().HasData(
                new Session
                {
                    Id = 1,
                    ProgramId = 1,
                    VenueId = 2
                }
            );
        }

        /*
         * Notes on seeding:
         * - Exercises are FKs in WorkoutExercises, and must be added before these.
         * - Clients are FKs in Programs, and must be added before these.
         * - Programs are FKs in Workouts, and must be added before these.
         * - Sessions are OPTIONAL FKs in Workouts, and must be added before these.
         *      - In the API, workouts can be created without the Session; it may be added later.
         * - Workouts are FKs in WorkoutExercises, and must be added before these.
         * - WorkoutExercises are FKs in ExerciseSets, and must be added before these.
         */
        public static void Seed(this ModelBuilder builder)
        {
            SeedVenueTypes(builder);
            SeedVenues(builder);

            SeedExercises(builder);
            SeedExerciseTypes(builder);
            SeedExerciseCategories(builder);

            SeedExerciseCategoryJoinTable(builder);
            SeedExerciseTypeJoinTable(builder);

            SeedClients(builder);
            SeedTrainers(builder);
            SeedAdmins(builder);

            // SeedPersons must come after tables that use Persons.
            SeedPersons(builder);


            SeedPrograms(builder);
            SeedSessions(builder);

            SeedWorkouts(builder);

            SeedWorkoutExercises(builder);

            SeedExerciseSets(builder);
        }
    }

}

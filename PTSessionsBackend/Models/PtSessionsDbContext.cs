﻿using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models.Domain;

namespace PTSessionsBackend.Models
{
    public class PtSessionsDbContext : DbContext
    {
        public DbSet<Exercise> Exercise { get; set; }

        public DbSet<ExerciseSet> ExerciseSet { get; set; }
       
        public DbSet<ExerciseCategory> ExerciseCategory { get; set; }

        public DbSet<PersonalBest> PersonalBest { get; set; }
       
        public DbSet<ExerciseType> ExerciseType { get; set; }
        
        public DbSet<Program> Program { get; set; }

        public DbSet<Person> Person { get; set; }

        public DbSet<Trainer> Trainer { get; set; }

        public DbSet<Admin> Admin { get; set; }

        public DbSet<Client> Client { get; set; }


        public DbSet<Venue> Venue { get; set; }

        public DbSet<VenueType> VenueType { get; set; }

        public DbSet<Workout> Workout { get; set; }

        public DbSet<WorkoutExercise> WorkoutExercise { get; set; }


        public DbSet<Session> Session { get; set; }

        public PtSessionsDbContext(DbContextOptions<PtSessionsDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Both Session and Program own Workout(s)
            // A session must belong to a Program
            // Hence, if a Session is deleted, we want to retain the Workout.
            
            // OTOH if a Program is deleted, the workouts will be cascade deleted
            // and we don't want the Session(also cascaded) to delete workouts, too.

            // TODO: Want Session FK set to null
            builder.Entity<Session>()
                .HasOne(s => s.Workout)
                .WithOne(w => w.Session)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Seed();
        }
    }
}

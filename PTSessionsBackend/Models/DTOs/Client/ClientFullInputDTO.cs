﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ClientFullInputDTO
    {
        [Required]
        public int Id { get; set; }

        [Required] 
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required]
        public string LastName { get; set; }
        public string Address { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DOB { get; set; }

        public float HeightCM { get; set; }
        public float WeightKG { get; set; }

        // TODO: Personal bests

        // Client may have multiple programs. Field below only necessary for UPDATE.
        public List<ProgramSimpleInputDTO> Programs { get; set; }
    }
}

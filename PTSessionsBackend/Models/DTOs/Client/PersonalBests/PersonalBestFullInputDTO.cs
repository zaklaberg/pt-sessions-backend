﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class PersonalBestFullInputDTO
    {
        public int Id { get; set; }
        
        public int ClientId { get; set; }

        [Required] 
        public string ExerciseName { get; set; }

        [Required]
        public int Value { get; set; }

        [Required]
        public string Unit { get; set; }

        [Required]
        public DateTime Achieved { get; set; }
    }
}

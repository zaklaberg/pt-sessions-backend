﻿using System;

namespace PTSessionsBackend.Models.DTOs
{
    public class PersonalBestFullOutputDTO
    {
        public int Id { get; set; }
        public ExerciseSimpleOutputDTO Exercise { get; set; }
        public int Value { get; set; }
        public string Unit { get; set; }
        public DateTime Achieved { get; set; }
    }
}
 
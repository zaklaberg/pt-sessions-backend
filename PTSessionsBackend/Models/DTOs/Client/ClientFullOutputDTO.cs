﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ClientFullOutputDTO
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; } 
        public string LastName { get; set; }
        public string Address { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DOB { get; set; }

        public float HeightCM { get; set; }
        public float WeightKG { get; set; }

        // TODO: Personal bests

        // Client may have multiple programs
        public List<ProgramSimpleOutputDTO> Programs { get; set; }
    }
}

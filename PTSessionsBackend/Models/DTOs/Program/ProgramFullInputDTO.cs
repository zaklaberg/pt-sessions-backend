﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ProgramFullInputDTO
    {
        public int Id { get; set; }

        public List<int> WorkoutIds { get; set; }

        [Required] 
        public string Name { get; set; }

        public string Description { get; set; }

        public int ClientId { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}

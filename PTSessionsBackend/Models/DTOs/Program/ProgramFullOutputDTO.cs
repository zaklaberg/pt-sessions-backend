﻿using System;
using System.Collections.Generic;

namespace PTSessionsBackend.Models.DTOs
{
    public class ProgramFullOutputDTO
    {
        public int Id { get; set; }

        public string Name { get; set; } 

        public string Description { get; set; }

        public ClientSimpleOutputDTO Client { get; set; }

        public List<WorkoutSimpleOutputDTO> Workouts { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}

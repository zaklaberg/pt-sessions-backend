﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.DTOs
{
    public class SessionFullInputDTO
    {
        public int Id { get; set; }
        [Required] 
        public DateTime Time { get; set; }

        [Required]
        public int ProgramId { get; set; }

        [Required]
        public int VenueId { get; set; }

        [Required]
        public int WorkoutId { get; set; }

        public bool Completed { get; set; }
    }
}

﻿using System;

namespace PTSessionsBackend.Models.DTOs
{
    public class SessionFullOutputDTO
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }

        public string ClientName { get; set; }

        public ProgramSimpleOutputDTO Program { get; set; } 

        public bool Completed { get; set; }
        
        public VenueSimpleOutputDTO Venue { get; set; }

        public WorkoutSimpleOutputDTO Workout { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class SessionSimpleOutputDTO
    {
        public DateTime Time { get; set; }

        public VenueSimpleOutputDTO Venue { get; set; } 

        public WorkoutSimpleOutputDTO Workout { get; set; }

        public string Url { get; set; }
    }
}

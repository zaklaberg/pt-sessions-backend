﻿using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.DTOs
{
    public class TrainerActivationInputDTO
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public bool Active { get; set; }
    }
} 

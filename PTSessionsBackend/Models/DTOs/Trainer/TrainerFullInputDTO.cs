﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.DTOs
{
    public class TrainerFullInputDTO
    {
        public int Id { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
         
        [Required]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public string ProfilePicture { get; set; }
    }
}

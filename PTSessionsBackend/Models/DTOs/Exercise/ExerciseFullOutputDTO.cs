﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ExerciseFullOutputDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        // Category describes the body area(s) the exercise targets. E.g. shoulders, biceps and so on.
        public List<ExerciseCategoryDTO> Categories { get; set; }

        // Type describes what the exercise is used for. E.g. used to build strength, endurance, balance and so on. 
        public List<ExerciseTypeDTO> Types { get; set; }
    }
}

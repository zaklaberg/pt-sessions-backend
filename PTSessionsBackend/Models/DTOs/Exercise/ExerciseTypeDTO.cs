﻿using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.DTOs
{
    public class ExerciseTypeDTO
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}
 
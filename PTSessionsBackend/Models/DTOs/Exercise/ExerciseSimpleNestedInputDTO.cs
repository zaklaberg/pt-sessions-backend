﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ExerciseSimpleNestedInputDTO
    {
        [Required]
        public int Id { get; set; }
        public List<WorkoutExerciseSetDTO> Sets { get; set; }
    }
}
 
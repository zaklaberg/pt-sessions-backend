﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class ExerciseFullInputDTO
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        // Category describes the body area(s) the exercise targets. E.g. shoulders, biceps and so on.
        [Required] 
        public List<ExerciseCategoryDTO> Categories { get; set; }

        // Type describes what the exercise is used for. E.g. used to build strength, endurance, balance and so on.
        [Required]
        public List<ExerciseTypeDTO> Types { get; set; }
    }
}

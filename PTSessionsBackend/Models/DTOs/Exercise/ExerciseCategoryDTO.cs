﻿using System.ComponentModel.DataAnnotations;

namespace PTSessionsBackend.Models.DTOs
{
    public class ExerciseCategoryDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
 
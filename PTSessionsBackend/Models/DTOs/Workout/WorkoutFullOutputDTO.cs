﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class WorkoutFullOutputDTO
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int Day { get; set; }
         
        public int Week { get; set; }
    }
}

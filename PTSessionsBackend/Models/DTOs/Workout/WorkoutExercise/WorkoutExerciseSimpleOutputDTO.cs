﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class WorkoutExerciseSimpleOutputDTO
    {
        public int Id { get; set; }
        public ExerciseSimpleOutputDTO Exercise { get; set; }
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class WorkoutExerciseSetDTO
    {
        public int Id { get; set; }

        public int NumberOfReps { get; set; }

        // Weight to be used, if applicable
        public int WeightKG { get; set; }

        // Time to rest after each rep, if applicable
        public int RestPeriodSeconds { get; set; }
         
        // Tempo describes [number of seconds down] - [number of seconds held at bottom] - [number of seconds up] - [number of seconds held at top]
        // E.g. for a pushup, a tempo might be: 2-0-2-0
        public string Tempo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class WorkoutExerciseFullOutputDTO
    {
        public int Id { get; set; }
        public ExerciseSimpleOutputDTO Exercise { get; set; }
        public List<WorkoutExerciseSetDTO> Sets { get; set; }
    }
}
 
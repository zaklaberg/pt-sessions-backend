﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class WorkoutFullInputDTO
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public int? ProgramId { get; set; }
         
        [Range(1, 7)]
        public int Day { get; set; }

        // A client with a non-repeating workout spanning more than 666 weeks 
        // should seriously reconsider life, the universe and everything
        [Range(1, 666)]
        public int Week { get; set; }
    }
}

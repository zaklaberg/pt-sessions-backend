﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models.DTOs
{
    public class VenueSimpleOutputDTO
    {
        public string Address { get; set; }
        public string Url { get; set; }
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Models
{
    public class AuthSettings
    {
        public static AuthSettings Data { get; set; }
        public string JwtSecret { get; set; }
        public string GoogleClientId { get; set; }
        public string GoogleClientSecret { get; set; } 
        public string JwtEmailEncryption { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace PTSessionsBackend.Middleware
{
    public class ErrorLogger
    {
        private readonly RequestDelegate _next;

        public ErrorLogger(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext ctx)
        {
            try
            {
                await _next(ctx);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine("T-ERROR: " + e.Message);
                throw;
            }
        }
    }
}

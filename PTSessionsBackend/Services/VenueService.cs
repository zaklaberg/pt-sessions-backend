﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Services
{
    public class VenueService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;
        public VenueService(PtSessionsDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<VenueFullOutputDTO>> Get()
        {
            var venues = await _context.Venue
                .Include(v => v.Type) 
                .ToListAsync();

            return venues
                .Select(v => _mapper.Map<VenueFullOutputDTO>(v))
                .ToList();
        }

        public async Task<VenueFullOutputDTO> Get(int id)
        {
            var venue = await _context.Venue
                .Include(v => v.Type)
                .FirstOrDefaultAsync(v => v.Id == id);

            if (venue == null)
                return null;

            return _mapper.Map<VenueFullOutputDTO>(venue);
        }

        public async Task<CRUD_SERVICE_RESULT> Delete(int id)
        {
            var venue = await _context.Venue.FindAsync(id);
            if (venue == null)
            {
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            }

            _context.Venue.Remove(venue);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Add(VenueFullInputDTO venueDto)
        {
            // TODO: Probably not necessary, .Add is enough
            venueDto.Id = 0;
            var venue = _mapper.Map<Venue>(venueDto);

            var venueType = await _context.VenueType
                .Include(vt => vt.Venues)
                .FirstOrDefaultAsync(vt => vt.Id == venueDto.TypeId);
            if (venueType == null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            venueType.Venues.Add(venue);
            await _context.SaveChangesAsync();

            venueDto.Id = venue.Id;

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<IEnumerable<VenueTypeDTO>> GetVenueTypes()
        {
            var types = await _context.VenueType
                .Select(vt => _mapper.Map<VenueTypeDTO>(vt))
                .ToListAsync();

            return types;
        }

        public async Task<CRUD_SERVICE_RESULT> Update(VenueFullInputDTO venueDto)
        {
            var venue = await _context.Venue
                .FindAsync(venueDto.Id);
            if (venue == null)
                return await Add(venueDto);

            // For many-to-one relationships, we can either change the FK in the dependent, or fiddle around with the lists in the owner.
            venue.TypeId = venueDto.TypeId;
            _mapper.Map(venueDto, venue);

            _context.Entry(venue).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VenueExists(venueDto.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        private bool VenueExists(int id)
        {
            return _context.Venue.Any(e => e.Id == id);
        }
    }
}

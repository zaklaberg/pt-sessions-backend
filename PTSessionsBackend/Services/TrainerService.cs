﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PTSessionsBackend.Services
{
    public class TrainerService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;

        public TrainerService(PtSessionsDbContext context, IMapper mapper)
        { 
            _mapper = mapper;
            _context = context;
        }

        public async Task<IEnumerable<Trainer>> Get()
        {
            return await _context.Trainer
                .Include(t => t.Person)
                .ToListAsync();
        }

        public async Task<Trainer> Get(int id)
        {
            return await _context.Trainer
               .Include(t => t.Person)
               .FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<CRUD_SERVICE_RESULT> Add(Trainer trainer)
        {
            // If a trainer with the given email exists, we error out
            if (_context.Trainer
                .Include(t => t.Person)
                .Any(t => t.Person.Email.ToLower() == trainer.Person.Email.ToLower()))
            {
                return CRUD_SERVICE_RESULT.BAD_REQUEST;
            }

            // Make sure we add person
            trainer.Person.Id = 0;
            _context.Trainer.Add(trainer);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Delete(int id)
        {
            var trainer = await _context.Trainer.FindAsync(id);
            if (trainer == null)
            {
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            }

            _context.Trainer.Remove(trainer);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> UpdateActiveStatus(int trainerId, bool active)
        {
            var trainer = await _context.Trainer.FindAsync(trainerId);
            if (trainer == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            
            // Update
            trainer.Active = active;
            _context.Entry(trainer).State = EntityState.Modified;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainerExists(trainer.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Update(TrainerFullInputDTO trainerDto)
        {
            // Get old trainer, if possible
            var trainer = await _context.Trainer
                .Include(t => t.Person)
                .FirstOrDefaultAsync(t => t.Id == trainerDto.Id);
            if (trainer == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            // Map over, doesn't overwrite clientId/adminId of Person
            _mapper.Map(trainerDto, trainer);

            _context.Entry(trainer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainerExists(trainer.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        private bool TrainerExists(int id)
        {
            return _context.Trainer.Any(e => e.Id == id);
        }

        public string SanitizedName(Trainer trainer)
        {
            Regex sanitizer = new Regex("[^a-z0-9-]", RegexOptions.IgnoreCase);
            var unsafeTrainerName = (trainer.Person.FirstName + trainer.Person.LastName).Replace(' ', '-');
            var safeTrainerName = sanitizer.Replace(unsafeTrainerName, "") + trainer.Id.ToString();

            return safeTrainerName;
        }
    }
}

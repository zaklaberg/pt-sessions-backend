﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Services
{
    /// <summary>
    /// Service is responsible for persisting files. When retrieving files, service will first attempt
    /// to locate them in the local path. If that fails, it will look in Azure storage using the filename 
    /// as blob lookup.
    /// </summary>
    public class StorageService
    {
        private string _connectionString;
        private const string containerClientName = "ptsessions-ccn";
        
        public StorageService(string connectionString)
        { 
            _connectionString = connectionString;   
        }

        /// <summary>
        /// Get a file from storage.
        /// </summary>
        /// <param name="filePath">The local path the file will be written to.</param>
        /// <param name="blobKey">Used to locate the file in storage.</param>
        /// <returns></returns>
        public async Task<bool> DownloadFile(string filePath, string blobKey)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_connectionString);

            var container = blobServiceClient.GetBlobContainerClient(containerClientName);
            await container.CreateIfNotExistsAsync();

            BlobClient client = container.GetBlobClient(blobKey);
            if (!client.Exists())
                return false;

            BlobDownloadInfo download = await client.DownloadAsync();
            using (FileStream downloadFileStream = File.OpenWrite(filePath))
            {
                await download.Content.CopyToAsync(downloadFileStream);
                downloadFileStream.Close();
            }

            return true;
        }

        /// <summary>
        /// Send a file to storage.
        /// </summary>
        /// <param name="filePath">The local path to read the file from.</param>
        /// <param name="blobKey">Used to locate the file in storage.</param>
        /// <returns></returns>
        public async Task<bool> UploadFile(string filePath, string blobKey)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_connectionString);

            var container = blobServiceClient.GetBlobContainerClient(containerClientName);
            await container.CreateIfNotExistsAsync();

            BlobClient client = container.GetBlobClient(blobKey);

            using FileStream uploadFileStream = File.OpenRead(filePath);
            await client.UploadAsync(uploadFileStream, true);
            uploadFileStream.Close();

            return true;
        }

        public async Task<List<string>> GetBlobs()
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_connectionString);

            var container = blobServiceClient.GetBlobContainerClient(containerClientName);
            await container.CreateIfNotExistsAsync();

            var blobs = new List<string>();
            await foreach (BlobItem blob in container.GetBlobsAsync())
            {
                blobs.Add(blob.Name);
            }

            return blobs;
        }
    }
}

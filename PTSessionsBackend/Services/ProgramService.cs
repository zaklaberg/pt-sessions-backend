﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Services
{
    public class ProgramService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;

        public ProgramService(PtSessionsDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        } 
        public async Task<IEnumerable<ProgramFullOutputDTO>> GetAll()
        {
            return await _context.Program
                .Include(p => p.Client)
                    .ThenInclude(c => c.Person)
                .Include(p => p.Workouts)
                .Select(p => _mapper.Map<ProgramFullOutputDTO>(p))
                .ToListAsync();
        }

        public async Task<ProgramFullOutputDTO> Get(int id)
        {
            var program = await _context.Program
                .Include(p => p.Client)
                    .ThenInclude(c => c.Person)
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<ProgramFullOutputDTO>(program);
        }

        public async Task<CRUD_SERVICE_RESULT> Update(ProgramFullInputDTO programDto)
        {
            // Get existing program
            var program = await _context.Program
                .Include(p => p.Client)
                    .ThenInclude(c => c.Programs)
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == programDto.Id);

            // Program has a new owner
            if (programDto.ClientId != program.ClientId)
            {
                // Make sure the new owner exists
                var newClient = await _context.Client.FirstOrDefaultAsync(c => c.Id == programDto.ClientId);
                if (newClient == null)
                    return CRUD_SERVICE_RESULT.BAD_REQUEST;

                // Remove program from old client (TODO: will this happen automagically when new is added?)
                program.Client.Programs.Remove(program);

                // Add new client( TODO: make 100% sure, but adding either is fine here)
                program.Client = newClient;
                program.ClientId = newClient.Id;
            }

            // Map over everything but client
            _mapper.Map(programDto, program);

            if (programDto.WorkoutIds != null)
            {
                // Workouts present in the current program but not in the dto are to be removed
                // For now we "soft remove" them, by deleting their session & nulling program.
                // This is so they can be re-used as bases, which have not been implemented.
                var existingWorkoutIds = program.Workouts.Select(w => w.Id).ToList();
                var workoutIdsToRemove = existingWorkoutIds.Except(programDto.WorkoutIds);

                foreach(var workoutId in workoutIdsToRemove)
                {
                    var workout = await _context.Workout.Include(w => w.Session).FirstOrDefaultAsync(w => w.Id == workoutId);
                    workout.Program = null;
                    workout.ProgramId = null;
                    workout.Session = null;
                    workout.SessionId = null;
                }

                var workoutIdsToAdd = programDto.WorkoutIds.Except(existingWorkoutIds);
                foreach (var workoutId in workoutIdsToAdd)
                {
                    var workout = await _context.Workout.FindAsync(workoutId);
                    if (workout == null || workout.ProgramId != null)
                        continue;
                    program.Workouts.Add(workout);
                }
            }

            _context.Entry(program).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramExists(programDto.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }
            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Add(ProgramFullInputDTO programDto)
        {
            // Make sure the supplied client exists
            var client = await _context.Client
                .Include(c => c.Programs)
                .FirstOrDefaultAsync(c => c.Id == programDto.ClientId);
            if (client == null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;
            
            // Map over
            var program = _mapper.Map<Program>(programDto);

            // Mark for insertion
            program.Id = 0;

            // Add
            client.Programs.Add(program);
            await _context.SaveChangesAsync();

            // Update id for use in CreatedAtAction
            programDto.Id = program.Id;

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Delete(int id)
        {
            var program = await _context.Program.FindAsync(id);
            if (program == null)
            {
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            }

            _context.Program.Remove(program);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<IEnumerable<SessionSimpleOutputDTO>> GetSessions(int programId)
        {
            var program = await _context.Program
                .Include(p => p.Sessions)
                    .ThenInclude(s => s.Venue)
                .Include(p => p.Sessions)
                    .ThenInclude(s => s.Workout)
                .FirstOrDefaultAsync(p => p.Id == programId);

            if (program == null)
                return null;

            return program.Sessions
                .Select(s => _mapper.Map<SessionSimpleOutputDTO>(s))
                .ToList();
        }

        public async Task<IEnumerable<WorkoutSimpleOutputDTO>> GetWorkouts(int programId)
        {
            var program = await _context.Program
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == programId);

            if (program == null)
                return null;

            return program.Workouts
                .Select(w => _mapper.Map<WorkoutSimpleOutputDTO>(w))
                .ToList();
        }

        public async Task<CRUD_SERVICE_RESULT> AddWorkout(int programId, WorkoutSimpleInputDTO workoutDto)
        {
            // Make sure program & workout exist
            var program = await _context.Program
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == programId);

            if (program == null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            var workout = await _context.Workout
                .Include(w => w.Program)
                .Include(w => w.Session)
                    .ThenInclude(s => s.Program)
                .FirstOrDefaultAsync(w => w.Id == workoutDto.Id);

            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            // If workout belongs to another program, do nothing
            if (workout.Program != null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // If workout belongs to another session not belonging to this program, do nothing
            if (workout.Session != null && workout.Session.ProgramId != programId)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // We have a fresh workout, register it to this program
            // TODO make sure this also gives the workout the correct FK
            program.Workouts.Add(workout);

            await _context.SaveChangesAsync();

            // Update id for use in CreatedAtAction
            workoutDto.Id = workout.Id;

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> RemoveWorkout(int programId, int workoutId)
        {
            // Make sure program & workout exist
            var program = await _context.Program
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == programId);

            if (program == null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            var workout = await _context.Workout
                .Include(w => w.Program)
                .Include(w => w.Session)
                    .ThenInclude(s => s.Program)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            // Clear the workout from program.
            // TODO: Make sure this also sets the FK to NULL!
            program.Workouts.Remove(workout);

            // Clear the corresponding session, if it exists
            if (workout.Session != null)
            {
                _context.Session.Remove(workout.Session);
                workout.Session = null;
            }

            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        private bool ProgramExists(int id)
        {
            return _context.Program.Any(e => e.Id == id);
        }
    }
}

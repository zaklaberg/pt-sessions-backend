﻿using System.Threading.Tasks;
using PTSessionsBackend.Models;
using Microsoft.EntityFrameworkCore;

namespace PTSessionsBackend.Services
{
    public class Role
    {
        public const string Admin = "admin";
        public const string Trainer = "trainer";
    }

    public class AuthUser
    {
        public string Name { get; set; } 
        public string Email { get; set; }

        public string Role { get; set; }
        public int TId { get; set; }
    }

    public interface IAuthService
    {
        Task<AuthUser> Authenticate(Google.Apis.Auth.GoogleJsonWebSignature.Payload payload);
    }

    public class AuthService : IAuthService
    {
        PtSessionsDbContext _context;
        public AuthService(PtSessionsDbContext context)
        {
            _context = context;
        }
        public async Task<AuthUser> Authenticate(Google.Apis.Auth.GoogleJsonWebSignature.Payload payload)
        {
            var admin = await _context.Admin
                .Include(a => a.Person)
                .FirstOrDefaultAsync(a => a.Person.Email.ToLower() == payload.Email.ToLower());

            // TODO. Not sure how Entity works w/ inheritance, ideally Client : Person, Trainer : Person and Admin : Person,
            // so we could just fetch a Person conditionally and write DRYer code below.
            if (admin != null)
            {
                var user = new AuthUser();
                user.Name = admin.Person.FirstName;
                user.Email = admin.Person.Email;
                user.Role = Role.Admin;
                user.TId = 0;

                return user;
            }

            var trainer = await _context.Trainer
                .Include(t => t.Person)
                .FirstOrDefaultAsync(t => t.Person.Email.ToLower() == payload.Email.ToLower());

            if (trainer != null)
            {
                var user = new AuthUser();
                user.Name = trainer.Person.FirstName + " ";
                if (trainer.Person.MiddleName != null)
                    user.Name += trainer.Person.MiddleName + " ";
                user.Name += trainer.Person.LastName;
                user.Email = trainer.Person.Email;
                user.Role = Role.Trainer;
                user.TId = trainer.Id;

                return user;
            }

            return null;
        }
    }
}

﻿using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace PTSessionsBackend.Services
{
    public class ClientService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;
        public ClientService(PtSessionsDbContext context, IMapper mapper)
        { 
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ClientFullOutputDTO>> Get()
        {
            return await _context.Client
                .Include(c => c.Programs)
                .Include(c => c.Person)
                .Select(c => _mapper.Map<ClientFullOutputDTO>(c))
                .ToListAsync();
        }

        public async Task<ClientFullOutputDTO> Get(int id)
        {
            var client = await _context.Client
                .Include(c => c.Programs)
                .Include(c => c.Person)
                .FirstAsync(c => c.Id == id);

            if (client == null)
                return null;

            return _mapper.Map<ClientFullOutputDTO>(client);
        }

        // Does NOT support adding new Programs here. If a non-existing program is supplied,
        // we return a BadRequest
        public async Task<CRUD_SERVICE_RESULT> Update(ClientFullInputDTO clientDto)
        {
            // Add if not existing
            if (!ClientExists(clientDto.Id))
                return await Add(clientDto);

            // Make sure supplied programs exist; bad request if not.
            if (clientDto.Programs != null)
            {
                foreach (var prog in clientDto.Programs)
                {
                    // If the program cant be found by id..
                    if (!_context.Program.Any(p => p.Id == prog.Id))
                    {
                        // See if one with a matching name exists
                        var programByName = await _context.Program.Where(p => p.Name == prog.Name).FirstOrDefaultAsync();

                        // No matching name, either. Off with the head!
                        if (programByName == null)
                            return CRUD_SERVICE_RESULT.BAD_REQUEST;

                        // A matching program was found. Great. Update the id
                        prog.Id = programByName.Id;
                    }
                }
            }
            
            // Get the old client
            var client = await _context.Client
                .Include(c => c.Programs)
                .Include(c => c.Person)
                .Where(c => c.Id == clientDto.Id)
                .FirstOrDefaultAsync();

            // Map in the DTO stuff, without many-to-many data
            _mapper.Map(clientDto, client);

            // Remove programs not present in the new client
            var programsToRemove = client.Programs
                .Where(program => !clientDto.Programs.Select(c => c.Id).Contains(program.Id))
                .ToList();
            // May want to actually DELETE these programs - if we do not want to reuse.
            // TODO
            programsToRemove.ForEach(program => client.Programs.Remove(program));

            // Add programs only present in the new client
            var programsToAdd = clientDto.Programs
                .Where(program => !client.Programs.Select(c => c.Id).Contains(program.Id))
                .ToList();

            programsToAdd.ForEach(program =>
            {
                // If a program with the given id exists, we 
                // - Remove it from the current owner, if there is one
                // - Remove the current owner from the Program
                // - Add current owner
                var existingProgram = _context.Program
                    .Include(p => p.Client)
                    .First(p => p.Id == program.Id);

                // Does this update the FK in the existing program? TODO
                if (existingProgram.Client != null)
                {
                    // Clear from old client
                    existingProgram.Client.Programs.Remove(existingProgram);

                    // Clear current FK, lets entity know to update it
                    existingProgram.Client = null;
                }

                client.Programs.Add(existingProgram);
            });

            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(client.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Add(ClientFullInputDTO clientDto)
        {
            // Enforce id
            clientDto.Id = 0;

            // Map over, ignoring Programs
            var client = _mapper.Map<Client>(clientDto);

            _context.Client.Add(client);
            try
            {
                await _context.SaveChangesAsync();

                // We'd like the new Id back if adding, for use in CreatedAtAction
                clientDto.Id = client.Id;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(client.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Delete(int id)
        {
            var client = await _context.Client.FindAsync(id);
            if (client == null)
            {
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            }

            _context.Client.Remove(client);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<PersonalBestFullOutputDTO> GetPersonalBest(int clientId, int pbId)
        {
            var pb = await _context.PersonalBest
                .Include(pb => pb.Exercise)
                .FirstOrDefaultAsync(pb => pb.Id == pbId);
            if (pb == null || pb.ClientId != clientId)
                return null;

            return _mapper.Map<PersonalBestFullOutputDTO>(pb);
        }

        public async Task<IEnumerable<PersonalBestFullOutputDTO>> GetPersonalBests(int clientId, int exerciseId = 0)
        {
            var client = await _context.Client
                .Include(c => c.PersonalBests)
                    .ThenInclude(pb => pb.Exercise)
                .FirstOrDefaultAsync(c => c.Id == clientId);

            if (client == null)
                return null;

            var pbs = client.PersonalBests.Select(pb => _mapper.Map<PersonalBestFullOutputDTO>(pb)).ToList();

            // Non-zero exerciseId => filter by it
            if (exerciseId != 0)
                pbs = pbs.Where(pb => pb.Exercise.Url == exerciseId.ToString()).ToList();

            return pbs;
        }

        public async Task<CRUD_SERVICE_RESULT> UpdatePersonalBest(PersonalBestFullInputDTO pbDto)
        {
            var pb = await _context.PersonalBest
                .FindAsync(pbDto.Id);

            if (pb == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            if (!ExerciseExists(pbDto.ExerciseName) || !ClientExists(pbDto.ClientId))
                return CRUD_SERVICE_RESULT.BAD_REQUEST;
            
            // TODO: What happens if the pb is re-assigned exercise/client? 
            // I'm 99% sure it gets removed from that client/exercise's list, but should check.
            _mapper.Map(pbDto, pb);

            // Map exerciseId manually
            pb.ExerciseId = _context.Exercise.Where(e => e.Name.ToLower() == pbDto.ExerciseName.ToLower()).First().Id;

            _context.Entry(pb).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PbExists(pb.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> AddPersonalBest(PersonalBestFullInputDTO pbDto)
        {
            // Make sure client & exercise exist
            if (!ClientExists(pbDto.ClientId) || !ExerciseExists(pbDto.ExerciseName))
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // Enforce id & map
            pbDto.Id = 0;
            var pb = _mapper.Map<PersonalBest>(pbDto);

            // Map exercise id manually
            pb.ExerciseId = _context.Exercise.Where(e => e.Name.ToLower() == pbDto.ExerciseName.ToLower()).First().Id;

            _context.PersonalBest.Add(pb);
            try
            {
                await _context.SaveChangesAsync();

                // We'd like the new Id back if adding, for use in CreatedAtAction
                pbDto.Id = pb.Id;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PbExists(pb.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public bool ClientExists(int id)
        {
            return _context.Client.Any(c => c.Id == id);
        }

        public bool ExerciseExists(string name)
        {
            return _context.Exercise.Any(e => e.Name.ToLower() == name.ToLower());
        }
        public bool ExerciseExists(int id)
        {
            return _context.Exercise.Any(e => e.Id == id);
        }

        public bool PbExists(int id)
        {
            return _context.PersonalBest.Any(pb => pb.Id == id);
        }
    }
}

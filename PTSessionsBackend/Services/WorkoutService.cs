﻿using AutoMapper;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models.Domain;

namespace PTSessionsBackend.Services
{
    public class WorkoutService
    {
        private readonly IMapper _mapper;
        private readonly PtSessionsDbContext _context;

        public WorkoutService(IMapper mapper, PtSessionsDbContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<WorkoutFullOutputDTO>> Get()
        { 
            var workouts =  await _context.Workout
                .Select(w => _mapper.Map<WorkoutFullOutputDTO>(w))
                .ToListAsync();

            return workouts;
        }

        public async Task<WorkoutFullOutputDTO> Get(int id)
        {
            var workout = await _context.Workout
                .FirstOrDefaultAsync(w => w.Id == id);

            return _mapper.Map<WorkoutFullOutputDTO>(workout);
        }

        public async Task<CRUD_SERVICE_RESULT> Add(WorkoutFullNestedInputDTO workoutDto)
        {
            // Mark for addition
            workoutDto.Id = 0;

            // If programId is specified, make sure it exists
            if (workoutDto.ProgramId != null)
            {
                if (!_context.Program.Any(p => p.Id == workoutDto.ProgramId))
                    return CRUD_SERVICE_RESULT.BAD_REQUEST;
            }

            // If exercises are given, make sure they exist
            if (workoutDto.Exercises != null)
            {
                foreach(var exercise in workoutDto.Exercises)
                {
                    if (!ExerciseExists(exercise.Id))
                        return CRUD_SERVICE_RESULT.BAD_REQUEST;
                }
            }

            // Add the workout itself
            var workout = _mapper.Map<Workout>(workoutDto);

            _context.Workout.Add(workout);
            await _context.SaveChangesAsync();

            workoutDto.Id = workout.Id;

            // Add workout exercises, if present
            foreach(var exercise in workoutDto.Exercises)
            {
                var addedExercise = new ExerciseSimpleInputDTO { Id = exercise.Id };
                var eResult = await AddExercise(workoutDto.Id, addedExercise);
                if (eResult != CRUD_SERVICE_RESULT.OK)
                    return eResult;

                // If sets are present, add those too
                if (exercise.Sets == null)
                    continue;

                foreach(var set in exercise.Sets)
                {
                    var esResult = await AddExerciseSet(workoutDto.Id, addedExercise.Id, set);
                    if (esResult != CRUD_SERVICE_RESULT.OK)
                        return esResult;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Update(WorkoutFullNestedInputDTO workoutDto)
        {
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                .FirstOrDefaultAsync(w => w.Id == workoutDto.Id);
            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            // Quick & dirty: delete all existing workoutexercises, then re-add from dto
            foreach(var workoutExercise in workout.WorkoutExercises)
            {
                workout.WorkoutExercises.Remove(workoutExercise);
            }
            _context.SaveChanges();

            // Add workout exercises, if present
            foreach (var exercise in workoutDto.Exercises)
            {
                var addedExercise = new ExerciseSimpleInputDTO { Id = exercise.Id };
                var eResult = await AddExercise(workoutDto.Id, addedExercise);
                if (eResult != CRUD_SERVICE_RESULT.OK)
                    return eResult;

                // If sets are present, add those too
                if (exercise.Sets == null)
                    continue;

                foreach (var set in exercise.Sets)
                {
                    var esResult = await AddExerciseSet(workoutDto.Id, addedExercise.Id, set);
                    if (esResult != CRUD_SERVICE_RESULT.OK)
                        return esResult;
                }
            }

            // Map the flat data
            _mapper.Map(workoutDto, workout);
            _context.Entry(workout).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkoutExists(workoutDto.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> AddExercise(int workoutId, ExerciseSimpleInputDTO exerciseDto)
        {
            // We don't allow adding non-existing exercises here,
            // to do that, post to /api/v1/exercises instead
            var exercise = _context.Exercise.Find(exerciseDto.Id);
            if (exercise == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                .FirstOrDefaultAsync(w => w.Id == workoutId);
            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            var workoutExercise = new WorkoutExercise
            {
                Id = 0,
                WorkoutId = workoutId,
                ExerciseId = exercise.Id
            };


            workout.WorkoutExercises.Add(workoutExercise);
            try
            {
                await _context.SaveChangesAsync();
                exerciseDto.Id = workoutExercise.Id;
            }
            catch(DbUpdateConcurrencyException)
            {
                if (!_context.WorkoutExercise.Any(we => we.Id == workoutExercise.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> DeleteExercise(int workoutId, int workoutExerciseId)
        {
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                .FirstOrDefaultAsync(w => w.Id == workoutId);
            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            
            var workoutExercise = workout.WorkoutExercises.FirstOrDefault(we => we.Id == workoutExerciseId);
            if (workoutExercise == null)
                return CRUD_SERVICE_RESULT.OK;

            workout.WorkoutExercises.Remove(workoutExercise);
            await _context.SaveChangesAsync();
            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<WorkoutExerciseFullOutputDTO> GetExercise(int workoutId, int workoutExerciseId)
        {
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Exercise)
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Sets)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return null;

            var exercise = workout.WorkoutExercises
                .FirstOrDefault(w => w.Id == workoutExerciseId);
            if (exercise == null)
                return null;

            return _mapper.Map<WorkoutExerciseFullOutputDTO>(exercise);
        }

        public async Task<List<WorkoutExerciseFullOutputDTO>> GetExercises(int workoutId)
        {
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Exercise)
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Sets)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return null;

            var exercises = workout.WorkoutExercises
                .Select(we => _mapper.Map<WorkoutExerciseFullOutputDTO>(we))
                .ToList();

            return exercises;
        }

        public async Task<List<WorkoutExerciseSetDTO>> GetExerciseSets(int workoutId, int workoutExerciseId)
        {
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Sets)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return null;

            var workoutExercise = workout.WorkoutExercises
                .FirstOrDefault(we => we.Id == workoutExerciseId);

            if (workoutExercise == null)
                return null;

            return workoutExercise.Sets
                .Select(set => _mapper.Map<WorkoutExerciseSetDTO>(set))
                .ToList();
        }

        public async Task<CRUD_SERVICE_RESULT> DeleteExerciseSet(int workoutId, int workoutExerciseId, int setId)
        {
            // Make sure supplied workout exercise exists
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Sets)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            var workoutExercise = workout.WorkoutExercises.FirstOrDefault(we => we.Id == workoutExerciseId);

            if (workoutExercise == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            var set = workoutExercise.Sets.FirstOrDefault(s => s.Id == setId);
            if (set == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            workoutExercise.Sets.Remove(set);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> AddExerciseSet(int workoutId, int workoutExerciseId, WorkoutExerciseSetDTO workoutExerciseSetDto)
        {
            // Make sure supplied workout exercise exists
            var workout = await _context.Workout
                .Include(w => w.WorkoutExercises)
                    .ThenInclude(we => we.Sets)
                .FirstOrDefaultAsync(w => w.Id == workoutId);

            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            var workoutExercise = workout.WorkoutExercises.FirstOrDefault(we => we.Id == workoutExerciseId);

            if (workoutExercise == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;
            
            // Map the static data
            var set = _mapper.Map<ExerciseSet>(workoutExerciseSetDto);
            // Mark for adding
            set.Id = 0;

            // Register it to current workout exercise
            set.WorkoutExerciseId = workoutExerciseId;

            workoutExercise.Sets.Add(set);

            try
            {
                await _context.SaveChangesAsync();
                workoutExerciseSetDto.Id = set.Id;
            }
            catch(DbUpdateConcurrencyException)
            {
                if (!_context.ExerciseSet.Any(es => es.Id == set.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }
            
            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Delete(int id)
        {
            // TODO: Deleting a workout should also delete its corresponding session.
            var workout = await _context.Workout.FindAsync(id);
            if (workout == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            _context.Workout.Remove(workout);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }

        public bool WorkoutExists(int id)
        {
            return _context.Workout.Any(w => w.Id == id);
        }

        public bool ExerciseExists(int id)
        {
            return _context.Exercise.Any(e => e.Id == id);
        }

        public bool ProgramExists(int id)
        {
            return _context.Program.Any(p => p.Id == id);
        }
    }
}

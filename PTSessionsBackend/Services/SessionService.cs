﻿using AutoMapper;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models.Domain;

namespace PTSessionsBackend.Services
{
    public class SessionService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;

        public SessionService(PtSessionsDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SessionFullOutputDTO>> GetAll() 
        {
            var sessions = await _context.Session
                .Include(s => s.Program)
                    .ThenInclude(s => s.Client)
                        .ThenInclude(c => c.Person)
                .Include(s => s.Venue)
                .Include(s => s.Workout)
                .Select(s => _mapper.Map<SessionFullOutputDTO>(s))
                .ToListAsync();

            return sessions;
        }

        public async Task<SessionFullOutputDTO> Get(int id)
        {
            var session = await _context.Session
                .Include(s => s.Program)
                    .ThenInclude(s => s.Client)
                        .ThenInclude(c => c.Person)
                .Include(s => s.Venue)
                .Include(s => s.Workout)
                .FirstOrDefaultAsync(s => s.Id == id);

            return _mapper.Map<SessionFullOutputDTO>(session);
        }

        public async Task<CRUD_SERVICE_RESULT> Update(SessionFullInputDTO sessionDto)
        {
            var session = await _context.Session.Include(s => s.Workout).FirstAsync(s => s.Id == sessionDto.Id);
            if (session == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            // Make sure the supplied ids exist
            if (!_context.Venue.Any(v => v.Id == sessionDto.VenueId) ||
                !_context.Workout.Any(w => w.Id == sessionDto.WorkoutId) || 
                !_context.Program.Any(p => p.Id == sessionDto.ProgramId))
            {
                return CRUD_SERVICE_RESULT.BAD_REQUEST;
            }

            // Session already has a workout registered; deregister it
            if (session.Workout != null)
            {
                session.Workout = null;
                await _context.SaveChangesAsync();
            }

            // Get the workout which we now want
            var workout = _context.Workout.Include(w => w.Session).First(s => s.Id == sessionDto.WorkoutId);

            // Register it
            session.Workout = workout;

            _mapper.Map(sessionDto, session);
            _context.Entry(session).State = EntityState.Modified;            

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SessionExists(session.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        public async Task<CRUD_SERVICE_RESULT> Add(SessionFullInputDTO sessionDto)
        {
            // Must contain a program and a workout
            var programExists = _context.Program.Any(p => p.Id == sessionDto.ProgramId);
            if (!programExists)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // If the workout isn't assigned to the program, the data is bad.
            var workout = await _context.Workout.FindAsync(sessionDto.WorkoutId);
            if (workout == null || workout.ProgramId != sessionDto.ProgramId)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // If the session is assigned a workout which already HAS a session, it's bad
            if (workout.SessionId != null)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // The supplied venue had better exist, too
            var venueExists = _context.Venue.Any(v => v.Id == sessionDto.VenueId);
            if (!venueExists)
                return CRUD_SERVICE_RESULT.BAD_REQUEST;

            // All good, FKs are valid, need only add Workout manually
            var session = _mapper.Map<Session>(sessionDto);
            session.Workout = workout;

            _context.Session.Add(session);
            await _context.SaveChangesAsync();

            // Update dto for createdataction
            sessionDto.Id = session.Id;

            return CRUD_SERVICE_RESULT.OK;
        }

        public bool SessionExists(int id)
        {
            return _context.Session.Any(s => s.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.Services
{
    public enum CRUD_SERVICE_RESULT
    {
        OK,
        NOT_FOUND,
        BAD_REQUEST
    }
}
 
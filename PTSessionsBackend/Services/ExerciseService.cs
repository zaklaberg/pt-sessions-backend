﻿using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using PTSessionsBackend.Models.DTOs;

namespace PTSessionsBackend.Services
{
    public class ExerciseService
    {
        private readonly PtSessionsDbContext _context;
        private readonly IMapper _mapper;

        public ExerciseService(PtSessionsDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        } 

        public async Task<IEnumerable<ExerciseFullOutputDTO>> GetExercises()
        {
            var exercises = await _context.Exercise
                .Include(e => e.Categories)
                .Include(e => e.Types)
                .ToListAsync();

            return exercises.Select(e => _mapper.Map<ExerciseFullOutputDTO>(e)).ToList();
        }

        public async Task<ExerciseFullOutputDTO> GetExerciseById(int id)
        {
            var exercise = await _context.Exercise
                .Include(e => e.Categories)
                .Include(e => e.Types)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
            
            return _mapper.Map<ExerciseFullOutputDTO>(exercise);
        }

        public async Task<CRUD_SERVICE_RESULT> DeleteExercise(int id)
        {
            var exercise = await _context.Exercise.FindAsync(id);
            if (exercise == null)
                return CRUD_SERVICE_RESULT.NOT_FOUND;

            _context.Exercise.Remove(exercise);
            await _context.SaveChangesAsync();

            return CRUD_SERVICE_RESULT.OK;
        }
        
        public async Task<IEnumerable<string>> GetExerciseTypes()
        {
            var types = await _context.ExerciseType.Select(e => e.Name).ToListAsync();
            return types;
        }

        public async Task<IEnumerable<string>> GetExerciseCategories()
        {
            var categs = await _context.ExerciseCategory.Select(e => e.Name).ToListAsync();
            return categs;
        }

        public async Task<CRUD_SERVICE_RESULT> Add(ExerciseFullInputDTO exerciseDto)
        {
            // Go to update, if we are updating
            if (ExerciseExists(exerciseDto.Id))
                return await Update(exerciseDto);

            // Set id to zero to let entity know that we are, in fact, adding
            exerciseDto.Id = 0;

            // Map over DTO, except the many-to-many data
            var newExercise = _mapper.Map<Exercise>(exerciseDto);

            // Get categories and types that are to be added
            var newCategories = exerciseDto.Categories.Select(c => _mapper.Map<ExerciseCategory>(c)).ToList();
            var newTypes = exerciseDto.Types.Select(t => _mapper.Map<ExerciseType>(t)).ToList();

            // Categories/types in the new exercise that already exist are
            // attached to context to let Entity know not to update/track them further.
            newExercise.Categories = newCategories.Select(categ =>
            {
                // If a matching id or name exists, then we want the corresponding category
                var fullCateg = _context.ExerciseCategory
                    .Where(ec => ec.Id == categ.Id || ec.Name.ToLower() == categ.Name.ToLower())
                    .FirstOrDefault();

                // Attach if category exists
                if (fullCateg != null)
                {
                    _context.ExerciseCategory.Attach(fullCateg);
                    return fullCateg;
                }
                // Otherwise add
                else
                {
                    // Entity wants id to be null or zero if adding, otherwise
                    // it thinks we want to insert the given id
                    categ.Id = 0;
                    return _context.ExerciseCategory.Add(categ).Entity;
                }
            }).ToList();

            newExercise.Types = newTypes.Select(type =>
            {
                var fullType = _context.ExerciseType
                    .Where(et => et.Id == type.Id || et.Name.ToLower() == type.Name.ToLower())
                    .FirstOrDefault();

                // Attach if type exists
                if (fullType != null)
                {
                    // Alternative to .Attach() is to just mark it as Unchanged
                    _context.Entry(fullType).State = EntityState.Unchanged;
                    return fullType;
                }
                // Otherwise add
                else
                {
                    // Entity wants id to be null or zero if adding, otherwise
                    // it thinks we explicitly want to insert the given id
                    type.Id = 0;
                    return _context.ExerciseType.Add(type).Entity;
                }
            }).ToList();

            // Add & save
            _context.Exercise.Add(newExercise);

            try
            {
                await _context.SaveChangesAsync();

                // We'd like the new Id back if adding, for use in CreatedAtAction
                exerciseDto.Id = newExercise.Id;

                // TODO: These may not be necessary, but... it's part of the response? check
                exerciseDto.Categories = _mapper.Map<List<ExerciseCategoryDTO>>(newExercise.Categories);
                exerciseDto.Types = _mapper.Map<List<ExerciseTypeDTO>>(newExercise.Types);
            }
            catch(DbUpdateConcurrencyException)
            {
                if (!ExerciseExists(exerciseDto.Id))
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                else
                    throw;
            }
            
            return CRUD_SERVICE_RESULT.OK;
        }

         /*        TEST CASES
          *        
          * - Updating EXISTING Exercise
          *      - Contains NEW categories/types(non-existing id || name, not contained in existing exercise)
          *      - Contains EXISTING categories/types IN existing exercise
          *      - Contains EXISTING categories/types IN database but NOT existing exercise   
         */
        public async Task<CRUD_SERVICE_RESULT> Update(ExerciseFullInputDTO exerciseDto)
        {
            // Add if not existing
            if (!ExerciseExists(exerciseDto.Id))
                return await Add(exerciseDto);

            // Get the old exercise
            var exercise = await _context.Exercise
                .Include(e => e.Categories)
                .Include(e => e.Types)
                .Where(e => e.Id == exerciseDto.Id)
                .FirstOrDefaultAsync();

            // Map in the DTO stuff, without many-to-many data
            _mapper.Map(exerciseDto, exercise);

            // Remove categories not present in the new exercise
            var categoriesToRemove = exercise.Categories
                .Where(categ =>
                {
                    bool containsId = exerciseDto.Categories.Select(c => c.Id).Contains(categ.Id);
                    bool containsName = exerciseDto.Categories.Select(c => c.Name).Contains(categ.Name);

                    return !containsId && !containsName;
                })
                .ToList();
            categoriesToRemove.ForEach(categ => exercise.Categories.Remove(categ));

            // Add categories only present in the new exercise
            var categoriesToAdd = exerciseDto.Categories
                .Where(categ =>
                {
                    bool containsId = exercise.Categories.Select(c => c.Id).Contains(categ.Id);
                    bool containsName = exercise.Categories.Select(c => c.Name).Contains(categ.Name);

                    return !containsId && !containsName;
                })
                .Select(categ => { categ.Id = 0; return _mapper.Map<ExerciseCategory>(categ); })
                .ToList();

            categoriesToAdd.ForEach(categ =>
            {
                // If a category with a similar name exists, we just attach the existing instance
                var existingCateg = _context.ExerciseCategory.FirstOrDefault(c => c.Name.ToLower() == categ.Name.ToLower());
                if (existingCateg == null)
                    exercise.Categories.Add(categ);
                else
                    exercise.Categories.Add(existingCateg);
            });

            // Remove types not present in the new exercise
            var typesToRemove = exercise.Types
                .Where(type =>
                {
                    bool containsId = exerciseDto.Types.Select(c => c.Id).Contains(type.Id);
                    bool containsName = exerciseDto.Types.Select(c => c.Name).Contains(type.Name);
                    return !containsId && !containsName;
                })
                .ToList();
            typesToRemove.ForEach(type => exercise.Types.Remove(type));

            // Add types only present in the new exercise
            var typesToAdd = exerciseDto.Types
                .Where(type =>
                {
                    bool containsId = exercise.Types.Select(c => c.Id).Contains(type.Id);
                    bool containsName = exercise.Types.Select(c => c.Name).Contains(type.Name);

                    return !containsId && !containsName;
                })
                .Select(type => { type.Id = 0; return _mapper.Map<ExerciseType>(type); })
                .ToList();

            typesToAdd.ForEach(type =>
            {
                // If a type with a similar name exists, we just attach the existing instance
                var existingType = _context.ExerciseType.FirstOrDefault(c => c.Name.ToLower() == type.Name.ToLower());
                if (existingType == null)
                    exercise.Types.Add(type);
                else
                    exercise.Types.Add(existingType);
            });

            _context.Entry(exercise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExerciseExists(exercise.Id))
                {
                    return CRUD_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }

            return CRUD_SERVICE_RESULT.OK;
        }

        private bool ExerciseExists(int id)
        {
            return _context.Exercise.Any(e => e.Id == id);
        }
    }
}

﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;

namespace PTSessionsBackend.MapperProfiles
{
    public class PersonalBestProfile : Profile
    {
        public PersonalBestProfile()
        {
            CreateMap<PersonalBest, PersonalBestFullOutputDTO>();

            CreateMap<PersonalBestFullInputDTO, PersonalBest>();
        } 
    }
}

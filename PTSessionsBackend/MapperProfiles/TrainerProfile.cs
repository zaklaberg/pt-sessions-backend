﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;

namespace PTSessionsBackend.MapperProfiles
{
    public class TrainerProfile : Profile
    {
        public TrainerProfile()
        {
            CreateMap<Trainer, TrainerFullOutputDTO>()
                .ForMember(dto => dto.FirstName, opt => opt.MapFrom(c => c.Person.FirstName))
                .ForMember(dto => dto.MiddleName, opt => opt.MapFrom(c => c.Person.MiddleName))
                .ForMember(dto => dto.LastName, opt => opt.MapFrom(c => c.Person.LastName))
                .ForMember(dto => dto.Email, opt => opt.MapFrom(c => c.Person.Email))
                .ForMember(dto => dto.Address, opt => opt.MapFrom(c => c.Person.Address))
                .ForMember(dto => dto.DOB, opt => opt.MapFrom(c => c.Person.DOB));

            CreateMap<TrainerFullInputDTO, Trainer>()
                .AfterMap((dto, c) =>
                {
                    if (c.Person == null)
                        c.Person = new Person();
                    c.Person.FirstName = dto.FirstName; 
                    c.Person.MiddleName = dto.MiddleName;
                    c.Person.LastName = dto.LastName;
                    c.Person.Email = dto.Email;
                    c.Person.Address = dto.Address;
                    c.Person.DOB = dto.DOB;
                });

            CreateMap<Trainer, TrainerFullInputDTO>()
                .ForMember(dto => dto.FirstName, opt => opt.MapFrom(c => c.Person.FirstName))
                .ForMember(dto => dto.MiddleName, opt => opt.MapFrom(c => c.Person.MiddleName))
                .ForMember(dto => dto.LastName, opt => opt.MapFrom(c => c.Person.LastName))
                .ForMember(dto => dto.Email, opt => opt.MapFrom(c => c.Person.Email))
                .ForMember(dto => dto.Address, opt => opt.MapFrom(c => c.Person.Address))
                .ForMember(dto => dto.DOB, opt => opt.MapFrom(c => c.Person.DOB));
        }
    }
}

﻿using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using AutoMapper;

namespace PTSessionsBackend.MapperProfiles
{
    public class ExerciseProfile : Profile
    {
        public ExerciseProfile()
        {
            // Simple bi-directional maps
            CreateMap<ExerciseCategory, ExerciseCategoryDTO>().ReverseMap();
            CreateMap<ExerciseType, ExerciseTypeDTO>().ReverseMap(); 

            // From Exercise to ExerciseFullOutputDTO
            CreateMap<Exercise, ExerciseFullOutputDTO>();

            // From Exercise to ExerciseSimpleOutputDTO. Map id into url, controller must finish the rest.
            CreateMap<Exercise, ExerciseSimpleOutputDTO>()
                .ForMember(dto => dto.Url, opt => opt.MapFrom(e => e.Id));

            // From ExerciseFullInputDTO to Exercise. We specifically do not map
            // the many-to-many relationships, as these require additional logic
            // when updating entries in the database(see ExerciseService::Update)
            CreateMap<ExerciseFullInputDTO, Exercise>()
                .ForMember(e => e.Categories, opt => opt.Ignore())
                .ForMember(e => e.Types, opt => opt.Ignore());
        }
    }
}

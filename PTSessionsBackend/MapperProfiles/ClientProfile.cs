﻿using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using AutoMapper;

namespace PTSessionsBackend.MapperProfiles
{
    public class ClientProfile : Profile
    {
        public ClientProfile()
        {
            // From Client -> ClientFullOutputDTO
            CreateMap<Client, ClientFullOutputDTO>()
                .ForMember(dto => dto.FirstName, opt => opt.MapFrom(c => c.Person.FirstName))
                .ForMember(dto => dto.MiddleName, opt => opt.MapFrom(c => c.Person.MiddleName))
                .ForMember(dto => dto.LastName, opt => opt.MapFrom(c => c.Person.LastName)) 
                .ForMember(dto => dto.Email, opt => opt.MapFrom(c => c.Person.Email))
                .ForMember(dto => dto.Address, opt => opt.MapFrom(c => c.Person.Address))
                .ForMember(dto => dto.DOB, opt => opt.MapFrom(c => c.Person.DOB));




            // From Client -> ClientSimpleOutputDTO. Map id to url, controller must finish the mapping
            CreateMap<Client, ClientSimpleOutputDTO>()
                .ForMember(cdto => cdto.Url, opt => opt.MapFrom(c => c.Id))
                .ForMember(cdto => cdto.Name, opt => opt.MapFrom(c => $"{c.Person.FirstName} {c.Person.LastName}"));

            // ClientFullInputDTO -> Client. Skip programs, 
            // these need manual treatment.
            CreateMap<ClientFullInputDTO, Client>()
                .AfterMap((dto, c) =>
                {
                    if (c.Person == null)
                        c.Person = new Person();

                    c.Person.FirstName = dto.FirstName;
                    c.Person.MiddleName = dto.MiddleName;
                    c.Person.LastName = dto.LastName;
                    c.Person.Email = dto.Email;
                    c.Person.Address = dto.Address;
                    c.Person.DOB = dto.DOB;
                })
                .ForMember(c => c.Programs, opt => opt.Ignore());
        }
    }
}

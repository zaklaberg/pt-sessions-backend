﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
namespace PTSessionsBackend.MapperProfiles
{
    public class SessionProfile : Profile
    {
        public SessionProfile()
        {
            CreateMap<Session, SessionFullOutputDTO>()
                .AfterMap((sess, dto) =>
                {
                    dto.Program.Url = sess.ProgramId.ToString();
                    dto.Venue.Url = sess.VenueId.ToString();
                    dto.Workout.Url = sess.Workout.Id.ToString();
                    dto.ClientName = sess.Program.Client.Person.FirstName + " " + sess.Program.Client.Person.LastName;
                });

            CreateMap<Session, SessionSimpleOutputDTO>()
                .ForMember(dto => dto.Url, opt => opt.MapFrom(s => s.Id))
                .AfterMap((sess, dto) =>
                {
                    dto.Workout.Url = sess.Workout.Id.ToString();
                    dto.Venue.Url = sess.VenueId.ToString(); 
                });

            CreateMap<SessionFullInputDTO, Session>();
        }
    }
}

﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;

namespace PTSessionsBackend.MapperProfiles
{
    public class WorkoutProfile : Profile
    {
        public WorkoutProfile()
        {
            // Subfield maps
            CreateMap<WorkoutExercise, WorkoutExerciseSimpleOutputDTO>();

            CreateMap<WorkoutExercise, WorkoutExerciseFullOutputDTO>();

            CreateMap<ExerciseSet, WorkoutExerciseSetDTO>().ReverseMap();

            // Map WorkoutDTO -> Workout, leaving many-to-many fields out
            // for manual update
            CreateMap<WorkoutFullInputDTO, Workout>()
                .ForMember(w => w.WorkoutExercises, opt => opt.Ignore());

            CreateMap<WorkoutFullNestedInputDTO, Workout>()
                .ForMember(w => w.WorkoutExercises, opt => opt.Ignore()); 

            CreateMap<Workout, WorkoutFullOutputDTO>();

            // Map id to url, controller must finish the mapping
            CreateMap<Workout, WorkoutSimpleOutputDTO>()
                .ForMember(wdto => wdto.Url, opt => opt.MapFrom(w => w.Id));
        }
    }
}

﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTSessionsBackend.MapperProfiles
{
    public class VenueProfile : Profile
    {
        public VenueProfile()
        {
            CreateMap<VenueType, VenueTypeDTO>();

            CreateMap<Venue, VenueFullOutputDTO>();
            CreateMap<Venue, VenueSimpleOutputDTO>()
                .ForMember(vdto => vdto.Url, opt => opt.MapFrom(v => v.Id));
            
            CreateMap<VenueFullInputDTO, Venue>();
        }
    }
} 

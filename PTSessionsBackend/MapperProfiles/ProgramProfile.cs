﻿using AutoMapper;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;

namespace PTSessionsBackend.MapperProfiles
{
    public class ProgramProfile : Profile
    {
        public ProgramProfile()
        {
            // Map Program -> ProgramDTO
            CreateMap<Program, ProgramFullOutputDTO>();

            // Map id to url first, then the controller will transform this to a proper url
            CreateMap<Program, ProgramSimpleOutputDTO>()
                .ForMember(pdto => pdto.Url, opt => opt.MapFrom(p => p.Id));

            // When mapping back, add the client manually 
            CreateMap<ProgramFullInputDTO, Program>()
                .ForMember(p => p.Client, opt => opt.Ignore());
        }
    }
}

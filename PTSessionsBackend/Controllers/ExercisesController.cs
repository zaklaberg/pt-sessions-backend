﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ExercisesController : ControllerBase
    { 
        private readonly ExerciseService _service;

        public ExercisesController(ExerciseService service)
        {
            _service = service;
        }

        /// <summary>
        /// Get all exercises
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Exercises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExerciseFullOutputDTO>>> GetExercises()
        {
            var exercises = await _service.GetExercises();
            return Ok(exercises);
        }

        /// <summary>
        /// Get a specific exercise
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If exercise is not found</response>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/v1/Exercises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ExerciseFullOutputDTO>> GetExercise(int id)
        {
            var exercise = await _service.GetExerciseById(id);
            
            if (exercise == null)
                return NotFound();
            return Ok(exercise);
        }

        /// <summary>
        /// Get registered exercise types
        /// </summary>
        /// <returns></returns>
        [HttpGet("types")]
        public async Task<ActionResult<List<string>>> GetTypes()
        {
            var types = await _service.GetExerciseTypes();
            return Ok(types);
        }

        /// <summary>
        /// Get registered exercise categories
        /// </summary>
        /// <returns></returns>
        [HttpGet("categories")]
        public async Task<ActionResult<List<string>>> GetCategories()
        {
            var categs = await _service.GetExerciseCategories();
            return Ok(categs);
        }

        /// <summary>
        /// Update an exercise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="exercise"></param>
        /// <response code="204">If the exercise was successfully updated</response> 
        /// <response code="400">If the ids in body and query do not match</response> 
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the database was interrupted</response>
        /// <returns></returns>
        // PUT: api/v1/Exercises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExercise(int id, ExerciseFullInputDTO exercise)
        {
            if (id != exercise.Id)
                return BadRequest();

            var result = await _service.Update(exercise);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();
            return NoContent();
        }

        /// <summary>
        /// Add a new exercise
        /// </summary>
        /// <param name="exerciseDto"></param>
        /// <remarks>
        /// For categories/types, giving an id will use the pre-existing category/type of that id, if it exists.
        /// If a name is given, the id parameter is not used, and a new category/type is created if necessary.
        /// </remarks>
        /// <response code="201">If the exercise was successfully added</response> 
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the database was interrupted</response> 
        /// <returns></returns>
        // POST: api/v1/Exercises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ExerciseFullOutputDTO>> PostExercise(ExerciseFullInputDTO exerciseDto)
        {
            var result = await _service.Add(exerciseDto);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            var exercise = await _service.GetExerciseById(exerciseDto.Id);
            return CreatedAtAction("GetExercise", new { id = exercise.Id }, exercise);
        }

        /// <summary>
        /// Delete an exercise
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If the exercise was successfully deleted</response> 
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/Exercises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExercise(int id)
        {
            await _service.DeleteExercise(id);
            return NoContent();
        }

        
    }
}

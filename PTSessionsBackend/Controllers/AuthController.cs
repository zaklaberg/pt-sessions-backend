﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using PTSessionsBackend.Models;
using PTSessionsBackend.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace PTSessionsBackend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// A dummy endpoint for authorization testing.
        /// </summary>
        /// <returns></returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("google")]
        public async Task<ActionResult> Google()
        {
            await Task.Delay(1);
            return Ok(new { Message = "Hello there! Did you maybe want to POST?" });
        }

        /// <summary>
        /// Return a signed JWT given a one-time token from Google.
        /// </summary>
        /// <param name="authDto"></param>
        /// <response code="200">If a JWT was successfully created</response> 
        /// <response code="401">If no user with corresponding email is present in the database</response> 
        /// <response code="500">If no token could be constructed</response> 
        /// <returns>A signed JWT</returns>
        [AllowAnonymous]
        [HttpPost("google")]
        public async Task<ActionResult> Google(AuthSimpleInputDTO authDto)
        {
            try
            {
                var payload = GoogleJsonWebSignature.ValidateAsync(authDto.TokenId, new GoogleJsonWebSignature.ValidationSettings()).Result;
                var user = await _authService.Authenticate(payload);

                if (user == null)
                    return Unauthorized();

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, Security.Encrypt(AuthSettings.Data.JwtEmailEncryption, user.Email)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("role", user.Role),
                    new Claim("tid", user.TId.ToString()),
                    new Claim("name", user.Name),
                };

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AuthSettings.Data.JwtSecret));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(string.Empty,
                  string.Empty,
                  claims,
                  expires: DateTime.Now.AddSeconds(5 * 60 * 60),
                  signingCredentials: creds);
                
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
            catch (Exception)
            {
                // Log error
                return StatusCode(500);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting; 
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TrainersController : ControllerBase
    {
        private readonly TrainerService _service;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly StorageService _storageService;

        public TrainersController(IMapper mapper, TrainerService service, IWebHostEnvironment hostingEnvironment, StorageService storageService)
        {
            _hostingEnvironment = hostingEnvironment;
            _service = service;
            _mapper = mapper;
            _storageService = storageService;
        }

        /// <summary>
        /// Get all trainers
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Trainers
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TrainerFullOutputDTO>>> GetTrainer()
        {
            var trainers = await _service.Get();
            return trainers.Select(t => _mapper.Map<TrainerFullOutputDTO>(t)).ToList();
        }

        // POST: api/v1/trainers/5/active
        /// <summary>
        /// Updates active status of a trainer. Only usable by admins.
        /// </summary>
        /// <param name="trainerId"></param>
        /// <param name="trainerDto"></param>
        /// <response code="204">On successful update</response>
        /// <response code="400">On mismatch between ids in body and query</response>
        /// <response code="401">If not authorized</response>
        /// <response code="403">If the user is authorized, but authorization level is insufficient</response> 
        /// <response code="404">If the trainer could not be found</response> 
        /// <returns>Nothing</returns>
        [HttpPost("{trainerId}/active")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
        public async Task<ActionResult> Activated(int trainerId, TrainerActivationInputDTO trainerDto)
        {
            if (trainerId != trainerDto.Id)
                return BadRequest();

            var result = await _service.UpdateActiveStatus(trainerDto.Id, trainerDto.Active);

            return result switch
            {
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Get a specific trainer
        /// </summary>
        /// <param name="id"></param>
        /// <response code="401">If not authorized</response>
        /// <returns>A trainer</returns>
        // GET: api/v1/Trainers/5
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public async Task<ActionResult<TrainerFullOutputDTO>> GetTrainer(int id)
        {
            var trainer = await _service.Get(id);
            if (trainer == null)
                return NotFound();

            return _mapper.Map<TrainerFullOutputDTO>(trainer);
        }

        /// <summary>
        /// Get the profile picture for a trainer
        /// </summary>
        /// <param name="id"></param>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/trainers/5/picture
        [HttpGet("{id}/picture")]
        public async Task<ActionResult> GetPicture(int id)
        {
            var trainer = await _service.Get(id);
            if (trainer == null)
                return NotFound();


            var image = Path.Combine(_hostingEnvironment.ContentRootPath, "Images", trainer.ProfilePicture);
            var contentType = $"image/{Path.GetExtension(trainer.ProfilePicture).Substring(1)}";

            // If the image doesn't exist locally, try to get it from storage
            if (!System.IO.File.Exists(image))
            {
                // Make sure Images folder exists
                var imagesPath = Path.Combine(_hostingEnvironment.ContentRootPath, "Images");
                if (!Directory.Exists(imagesPath))
                    Directory.CreateDirectory(imagesPath);

                var result = await _storageService.DownloadFile(image, Path.GetFileName(image));
                if (!result)
                    return NotFound();
            }

            return PhysicalFile(image, contentType);
        }

        /// <summary>
        /// Add a profile picture to a trainer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <response code="201">If the picture was successfully added</response> 
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the trainer could not be found</response> 
        /// <response code="500">If the picture could not be saved</response> 
        /// <returns>An URL to where the picture can be fetched</returns>
        // POST: api/v1/trainers/5/picture
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("{id}/picture")]
        public async Task<ActionResult> AddOrUpdateTrainerPicture(int id, [FromForm] IFormFile file)
        {
            var trainer = await _service.Get(id);
            if (trainer == null)
                return NotFound();

            var safeTrainerName = _service.SanitizedName(trainer);

            // Make sure Images folder exists
            var imagesPath = Path.Combine(_hostingEnvironment.ContentRootPath, "Images");
            if (!Directory.Exists(imagesPath))
                Directory.CreateDirectory(imagesPath);

            try 
            { 
                string path = Path.Combine(_hostingEnvironment.ContentRootPath, "Images", safeTrainerName + Path.GetExtension(file.FileName));
                
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                await _storageService.UploadFile(path, Path.GetFileName(path));

                var trainerDto = _mapper.Map<TrainerFullInputDTO>(trainer);
                trainerDto.ProfilePicture = safeTrainerName + Path.GetExtension(file.FileName);
                var result = await _service.Update(trainerDto);
            }
            catch(Exception e)
            {
                System.Diagnostics.Trace.TraceError(e.Message);
                System.Diagnostics.Trace.TraceError(e.StackTrace);
                return StatusCode(500);
            }
            
            var url = Url.Action("GetPicture", "Trainers", new { Id = id}, Request.Scheme);
            return Created(url, new { Id = id });
        }

        /// <summary>
        /// Update an existing trainer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="trainerDto"></param>
        /// <response code="400">If ids in body and query do not match</response> 
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the trainer could not be found</response> 
        /// <response code="204">If the trainer was successfully updated</response> 
        /// <returns></returns>
        // PUT: api/v1/Trainers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrainer(int id, TrainerFullInputDTO trainerDto)
        {
            if (id != trainerDto.Id)
                return BadRequest();

            var result = await _service.Update(trainerDto);
            return result switch
            {
                CRUD_SERVICE_RESULT.BAD_REQUEST => BadRequest(),
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Add a trainer
        /// </summary>
        /// <param name="trainerDto"></param>
        /// <response code="201">If the trainer was successfully added</response> 
        /// <response code="400">If the email is already registered</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // POST: api/v1/Trainers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<ActionResult<Trainer>> PostTrainer(TrainerFullInputDTO trainerDto)
        {
            var result = await _service.Add(_mapper.Map<Trainer>(trainerDto));
            if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();

            var trainer = await _service.Get(trainerDto.Id);
            return CreatedAtAction("GetTrainer", new { id = trainer.Id }, trainer);
        }

        /// <summary>
        /// Delete a trainer
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If the trainer was successfully deleted</response> 
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/Trainers/5
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrainer(int id)
        {
            await _service.Delete(id);
            return NoContent();
        }
    }
}

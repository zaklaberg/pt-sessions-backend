﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers
{ 
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class WorkoutsController : ControllerBase
    {
        private readonly WorkoutService _service;

        public WorkoutsController(WorkoutService service)
        {
            _service = service;
        }

        /// <summary>
        /// Get all workouts 
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/Workouts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkoutFullOutputDTO>>> GetWorkout()
        {
            return Ok(await _service.Get());
        }

        /// <summary>
        /// Get a specific workout
        /// </summary>
        /// <param name="id"></param>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/Workouts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkoutFullOutputDTO>> GetWorkout(int id)
        {
            var workout = await _service.Get(id);

            if (workout == null)
                return NotFound();

            return Ok(workout);
        }

        /// <summary>
        /// Get all the exercises in a workout
        /// </summary>
        /// <param name="workoutId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout is not found</response>
        /// <returns></returns>
        // GET: api/v1/workouts/{workoutId}/exercises
        [HttpGet("{workoutId}/exercises")]
        public async Task<ActionResult<List<WorkoutExerciseFullOutputDTO>>> GetWorkoutExercises(int workoutId)
        {
            var exercises = await _service.GetExercises(workoutId);
            if (exercises == null)
                return NotFound();

            foreach (var exercise in exercises)
                AddOutputURLs(exercise);

            return Ok(exercises);
        }

        /// <summary>
        /// Get a specific workout exercise
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout or exercise could not be found</response>
        /// <returns></returns>
        [HttpGet("{workoutId}/exercises/{workoutExerciseId}")]
        public async Task<ActionResult<List<WorkoutExerciseFullOutputDTO>>> GetWorkoutExercise(int workoutId, int workoutExerciseId)
        {
            var exercise = await _service.GetExercise(workoutId, workoutExerciseId);
            if (exercise == null)
                return NotFound();

            AddOutputURLs(exercise);
            return Ok(exercise);
        }

        /// <summary>
        /// Add an (existing) exercise to workout
        /// </summary>
        /// <remarks>
        /// Note that the exercise itself should already exist(see /api/v1/exercises).
        /// Adding the exercise to the workout allows subsequently adding sets to it.
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="exerciseDto"></param>
        /// <response code="201">If exercise successfully added</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If either workout or exercise do not exist</response>
        /// <returns></returns>
        // POST: api/v1/workouts/{workoutId}/exercises
        [HttpPost("{workoutId}/exercises")]
        public async Task<ActionResult> PostExercise(int workoutId, ExerciseSimpleInputDTO exerciseDto)
        {
            var result = await _service.AddExercise(workoutId, exerciseDto);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            HttpRequest request = Url.ActionContext.HttpContext.Request;
            var uri = new Uri(new Uri(request.Scheme + "://" + request.Host.Value), Url.Content($"api/v1/workouts/{workoutId}/exercises/{exerciseDto.Id}")).ToString();
            return Created(uri, new { Id = exerciseDto.Id });
        }

        /// <summary>
        /// Removes an exercise from workout
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <response code="204">If exercise was successfully removed</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout does not exist</response>
        /// <returns></returns>
        // DELETE: api/v1/workouts/{workoutId}/exercises/{exerciseId}
        [HttpDelete("{workoutId}/exercises/{workoutExerciseId}")]
        public async Task<ActionResult> DeleteExercise(int workoutId, int workoutExerciseId)
        {
            var result = await _service.DeleteExercise(workoutId, workoutExerciseId);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Get all sets for an exercise in workout
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/workouts/{workoutId}/exercises/{exerciseId}/sets
        [HttpGet("{workoutId}/exercises/{workoutExerciseId}/sets")]
        public async Task<ActionResult<List<WorkoutExerciseSetDTO>>> GetWorkoutExerciseSets(int workoutId, int workoutExerciseId)
        {
            var sets = await _service.GetExerciseSets(workoutId, workoutExerciseId);
            return Ok(sets);
        }

        /// <summary>
        /// Get a specific exercise set
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <param name="setId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout, workout exercise or set could not be found</response>
        /// <returns></returns>
        [HttpGet("{workoutId}/exercises/{workoutExerciseId}/sets/{setId}")]
        public async Task<ActionResult<WorkoutExerciseSetDTO>> GetWorkoutExerciseSet(int workoutId, int workoutExerciseId, int setId)
        {
            var sets = await _service.GetExerciseSets(workoutId, workoutExerciseId);
            if (sets == null)
                return NotFound();
            var set = sets.FirstOrDefault(s => s.Id == setId);
            if (set == null)
                return NotFound();
            return Ok(set);
        }

        /// <summary>
        /// Add a new set to an exercise in workout
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <param name="workoutExerciseSetDto"></param>
        /// <response code="201">If exercise was successfully added</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout or exercise is not found, or database was interrupted</response>
        /// <returns></returns>
        // POST: api/v1/workouts/{workoutId}/exercises/{exerciseId}
        [HttpPost("{workoutId}/exercises/{workoutExerciseId}/sets")]
        public async Task<ActionResult> PostWorkoutExerciseSet(int workoutId, int workoutExerciseId, WorkoutExerciseSetDTO workoutExerciseSetDto)
        {
            var result = await _service.AddExerciseSet(workoutId, workoutExerciseId, workoutExerciseSetDto);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            HttpRequest request = Url.ActionContext.HttpContext.Request;
            var uri = new Uri(new Uri(request.Scheme + "://" + request.Host.Value), Url.Content($"api/v1/workouts/{workoutId}/exercises/{workoutExerciseId}/sets/{workoutExerciseSetDto.Id}")).ToString();
            return Created(uri, new { Id = workoutExerciseSetDto });
        }

        /// <summary>
        /// Removes a set from an exercise in workout
        /// </summary>
        /// <param name="workoutId"></param>
        /// <param name="workoutExerciseId"></param>
        /// <param name="setId"></param>
        /// <response code="204">If set was successfully removed</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/workouts/{workoutId}/exercises/{workoutExerciseId}/sets/{setId}
        [HttpDelete("{workoutId}/exercises/{workoutExerciseId}/sets/{setId}")]
        public async Task<ActionResult> DeleteWorkoutExerciseSet(int workoutId, int workoutExerciseId, int setId)
        {
            await _service.DeleteExerciseSet(workoutId, workoutExerciseId, setId);

            return NoContent();
        }

        /// <summary>
        /// Update a workout
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workout"></param>
        /// <response code="204">If workout was successfully updated</response>
        /// <response code="400">If ids in body and query do not match</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If workout is not found or database was interrupted</response>
        /// <returns></returns>
        // PUT: api/Workouts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkout(int id, WorkoutFullNestedInputDTO workout)
        {
            if (id != workout.Id)
                return BadRequest();

            var result = await _service.Update(workout);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Add a new workout
        /// </summary>
        /// <remarks>
        /// If programId is specified, the corresponding Program is assumed to exist. If it does not, a 400 bad request is returned.
        /// </remarks>
        /// <param name="workout"></param>
        /// <response code="201">If workout was successfully added</response>
        /// <response code="400">If a programId to a non-existing program was supplied</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // POST: api/Workouts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<WorkoutFullOutputDTO>> PostWorkout(WorkoutFullNestedInputDTO workout)
        {
            var result = await _service.Add(workout);

            if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();

            var addedWorkout = await _service.Get(workout.Id);
            return CreatedAtAction("GetWorkout", new { id = workout.Id }, addedWorkout);
        }

        /// <summary>
        /// Delete a workout
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If successfully removed</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/Workouts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout(int id)
        {
            await _service.Delete(id);
            return NoContent();
        }

        // Not very dry!
        private void AddOutputURLs(WorkoutExerciseFullOutputDTO we)
        {
            we.Exercise.Url = Url.Action("GetExercise", "Exercises", new { Id = we.Exercise.Url }, Request.Scheme);
        }
        private void AddOutputURLs(WorkoutExerciseSimpleOutputDTO we)
        {
            we.Exercise.Url = Url.Action("GetExercise", "Exercises", new { Id = we.Exercise.Url }, Request.Scheme);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProgramsController : ControllerBase
    {
        private readonly ProgramService _programService;

        public ProgramsController(ProgramService programService)
        {
            _programService = programService;
        }

        /// <summary>
        /// Get all programs
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Programs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProgramFullOutputDTO>>> GetProgram()
        {
            var programs = await _programService.GetAll();
            foreach (var program in programs)
            {
                AddOutputURLs(program);
                foreach (var workout in program.Workouts)
                    AddOutputURLs(workout);
            }
            return Ok(programs);
        }

        /// <summary>
        /// Get a specific program
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/v1/Programs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProgramFullOutputDTO>> GetProgram(int id)
        {
            var program = await _programService.Get(id);

            if (program == null)
                return NotFound();

            AddOutputURLs(program);
            foreach (var workout in program.Workouts)
                AddOutputURLs(workout);
            return program;
        }

        /// <summary>
        /// Update an existing program
        /// </summary>
        /// <param name="id"></param>
        /// <param name="program"></param>
        /// <response code="204">If program was successfully updated</response>
        /// <response code="400">If ids in body and query do not match</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If database is interrupted</response>
        /// <returns></returns>
        // PUT: api/v1/Programs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgram(int id, ProgramFullInputDTO program)
        {
            if (id != program.Id)
                return BadRequest();

            var result = await _programService.Update(program);

            return result switch
            {
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Add a new program
        /// </summary>
        /// <param name="programDto"></param>
        /// <response code="201">If program was successfully added</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If database is interrupted</response>
        /// <returns></returns>
        // POST: api/v1/Programs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProgramFullOutputDTO>> PostProgram(ProgramFullInputDTO programDto)
        {
            var result = await _programService.Add(programDto);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            var program = await _programService.Get(programDto.Id);
            AddOutputURLs(program);

            return CreatedAtAction("GetProgram", new { id = program.Id }, program);
        }

        /// <summary>
        /// Delete a program
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If program successfully deleted</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/Programs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProgram(int id)
        {
            await _programService.Delete(id);
            return NoContent();
        } 

        /// <summary>
        /// Get all sessions registered in the program
        /// </summary>
        /// <param name="programId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If program could not be found</response>
        /// <returns></returns>
        // GET: api/v1/programs/{programId}/sessions
        [HttpGet("{programId}/sessions")]
        public async Task<ActionResult<IEnumerable<SessionFullOutputDTO>>> GetSessions(int programId)
        {
            var sessions = await _programService.GetSessions(programId);
            if (sessions == null)
                return NotFound();

            foreach (var session in sessions)
                AddOutputURLs(session);
            return Ok(sessions);
        }

        // POST: api/v1/programs/{programId}/sessions
        // Purpose: register an existing session to this program
        // NotImplemented: sessions are PRESUMABLY always created after programs,
        // hence we can add a programId when POSTing to /sessions instead.

        // DELETE: api/v1/programs/{programId}/sessions
        // Purpose: remove a session from this program
        // NotImplemented: DELETE to sessions/ should do the same thing

        /// <summary>
        /// Get all workouts in this program
        /// </summary>
        /// <param name="programId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If program could not be found</response>
        /// <returns></returns>
        // GET: api/v1/programs/{programId}/workouts
        [HttpGet("{programId}/workouts")]
        public async Task<ActionResult<IEnumerable<WorkoutSimpleOutputDTO>>> GetWorkouts(int programId)
        {
            var workouts = await _programService.GetWorkouts(programId);
            if (workouts == null)
                return NotFound();
            foreach (var workout in workouts)
                AddOutputURLs(workout);
            return Ok(workouts);
        }

        /// <summary>
        /// Register an existing workout to the given program.
        /// </summary>
        /// <param name="programId"></param>
        /// <param name="workoutDto"></param>
        /// <response code="404">If workout or program is not found</response>
        /// <response code="400">If the supplied workout belongs to another program, either directly or indirectly through another program's session</response>
        /// <response code="401">If not authorized</response>
        /// <response code="201">If the workout was successfully registered to the program</response>
        /// <returns></returns>
        // POST: api/v1/programs/{programId}/workouts
        [HttpPost("{programId}/workouts")]
        public async Task<ActionResult<WorkoutFullOutputDTO>> PostWorkout(int programId, WorkoutSimpleInputDTO workoutDto)
        {
            var result = await _programService.AddWorkout(programId, workoutDto);
            if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();
            else if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            // TODO get a workout from workoutservice, the line below
            // duplicates addoutputurls
            var url = Url.Action("GetWorkout", "Workouts", new { Id = workoutDto.Id }, Request.Scheme);
            return Created(url, workoutDto);
        }

        /// <summary>
        /// Unregister the workout from the program
        /// </summary>
        /// <param name="programId"></param>
        /// <param name="workoutId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the program or workout is not found</response>
        /// <response code="201">If the workout was successfully unregistered</response>
        /// <returns></returns>
        // DELETE: api/v1/programs/{programId}/workouts
        // Unregisters the workout from the current program.
        // Should also delete the corresponding session if it exists, or 
        // at least mark as obsolete.
        [HttpDelete("{programId}/workouts/{workoutId}")]
        public async Task<ActionResult> DeleteWorkout(int programId, int workoutId)
        {
            var result = await _programService.RemoveWorkout(programId, workoutId);
            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();
            return NoContent();
        }


        private void AddOutputURLs(ProgramFullOutputDTO program)
        {
            program.Client.Url = Url.Action("GetClient", "Clients", new { Id = program.Client.Url }, Request.Scheme);
        }

        private void AddOutputURLs(SessionSimpleOutputDTO session)
        {
            session.Url = Url.Action("GetSession", "Sessions", new { Id = session.Url }, Request.Scheme);
            session.Venue.Url = Url.Action("GetVenue", "Venues", new { Id = session.Venue.Url }, Request.Scheme);
            session.Workout.Url = Url.Action("GetWorkout", "Workouts", new { Id = session.Workout.Url }, Request.Scheme);
        }

        private void AddOutputURLs(WorkoutSimpleOutputDTO workout)
        {
            workout.Url = Url.Action("GetWorkout", "Workouts", new { Id = workout.Url }, Request.Scheme);
        }
    }
}

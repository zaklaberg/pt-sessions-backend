﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.DTOs;
using AutoMapper;
using PTSessionsBackend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
 
namespace PTSessionsBackend.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class VenuesController : ControllerBase
    {
        private readonly PtSessionsDbContext _context;
        private readonly VenueService _service;

        public VenuesController(PtSessionsDbContext context, VenueService service)
        {
            _service = service;
            _context = context;
        }

        /// <summary>
        /// Get all venues
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Venues
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VenueFullOutputDTO>>> GetVenue()
        {
            var venues = await _service.Get();
            return Ok(venues);
        }

        /// <summary>
        /// Get a specific venue
        /// </summary>
        /// <param name="id"></param>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Venues/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VenueFullOutputDTO>> GetVenue(int id)
        {
            var venue = await _service.Get(id);
            if (venue == null)
                return NotFound();
            return Ok(venue);
        }

        /// <summary>
        /// Get all venue types
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/venues/types
        [HttpGet("types")]
        public async Task<ActionResult<IEnumerable<VenueTypeDTO>>> GetVenueTypes()
        {
            var types = await _service.GetVenueTypes();
            return Ok(types);
        }

        /// <summary>
        /// Update a specific venue
        /// </summary>
        /// <param name="id"></param>
        /// <param name="venueDto"></param>
        /// <response code="400">If ids in body and query do not match</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If database was interrupted</response>
        /// <response code="204">If venue was successfully updated</response>
        /// <returns></returns>
        // PUT: api/v1/Venues/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVenue(int id, VenueFullInputDTO venueDto)
        {
            if (id != venueDto.Id)
                return BadRequest();

            var result = await _service.Update(venueDto);
            return result switch
            {
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Add a new venue
        /// </summary>
        /// <param name="venueDto"></param>
        /// <response code="204">If venue was successfully created</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If database was interrupted</response>
        /// <returns></returns>
        // POST: api/v1/Venues
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<VenueFullOutputDTO>> PostVenue(VenueFullInputDTO venueDto)
        {
            var result = await _service.Add(venueDto);
            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            return CreatedAtAction("GetVenue", new { id = venueDto.Id }, venueDto);
        }

        /// <summary>
        /// Delete a venue
        /// </summary>
        /// <param name="id"></param>
        /// <response code="201">If venue successfully deleted</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/Venues/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVenue(int id)
        {
            await _service.Delete(id);
            return NoContent();
        }

    }
}

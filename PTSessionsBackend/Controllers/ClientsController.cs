﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.Domain;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientService _service;

        public ClientsController(ClientService service)
        {
            _service = service;
        }

        /// <summary>
        /// Get all clients
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Clients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientFullOutputDTO>>> GetClient()
        {
            var clients = await _service.Get();
            foreach (var client in clients)
                AddOutputURLs(client);
            return Ok(clients);
        }

        /// <summary>
        /// Get a specific client
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If client is not found</response>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/v1/Clients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClientFullOutputDTO>> GetClient(int id)
        {
            var client = await _service.Get(id);
            if (client == null)
                return NotFound();
            AddOutputURLs(client);
            return Ok(client);
        }

        /// <summary>
        /// Get personal bests for client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="filterBy">Specify [exercises] to filter by exercise</param>
        /// <param name="exercise">The id of an exercise to filter by</param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If client is not found</response>
        /// <returns></returns>
        [HttpGet("{clientId}/personal-bests")]
        public async Task<ActionResult<IEnumerable<PersonalBestFullOutputDTO>>> GetPersonalBests(int clientId, [FromQuery] string filterBy, [FromQuery] int exercise = 0)
        {
            var pbs = await _service.GetPersonalBests(clientId, exercise);
            if (pbs == null)
                return NotFound();


            foreach (var pb in pbs)
                AddOutputURLs(pb);

            return Ok(pbs);
        }

        /// <summary>
        /// Get a specific personal best
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="pbId"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If client or personal best is not found</response>
        /// <returns></returns>
        [HttpGet("{clientId}/personal-bests/{pbId}")]
        public async Task<ActionResult<PersonalBestFullOutputDTO>> GetPersonalBest(int clientId, int pbId)
        {
            var pb = await _service.GetPersonalBest(clientId, pbId);
            if (pb == null)
                return NotFound();
            AddOutputURLs(pb);

            return Ok(pb);
        }

        /// <summary>
        /// Add a new personal best to client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="pbDto"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="400">If ids in query and body do not match, or if client/exercise supplied does not exist.</response>
        /// <response code="404">If client is not found</response>
        /// <returns></returns>
        [HttpPost("{clientId}/personal-bests")]
        public async Task<ActionResult<PersonalBestFullOutputDTO>> AddPersonalBest(int clientId, PersonalBestFullInputDTO pbDto)
        {
            pbDto.ClientId = clientId;

            var result = await _service.AddPersonalBest(pbDto);

            if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();
            else if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            // Get updated pb
            var pb = await _service.GetPersonalBest(clientId, pbDto.Id);
            if (pb == null)
                return NotFound();
            AddOutputURLs(pb);

            HttpRequest request = Url.ActionContext.HttpContext.Request;
            var uri = new Uri(new Uri(request.Scheme + "://" + request.Host.Value), Url.Content($"api/v1/clients/{clientId}/personal-bests/{pb.Id}")).ToString();
            return Created(uri, pb);
        }

        /// <summary>
        /// Update an existing personal best
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="pbDto"></param>
        /// <response code="204">If successfully updated</response>
        /// <response code="400">If client id in query/body do not match, or if supplied client or exercise do not exist</response>
        /// <response code="404">If personal best does not exist</response>
        /// <returns></returns>
        [HttpPut("{clientId}/personal-bests/{pbId}")]
        public async Task<ActionResult> UpdatePersonalBest(int clientId, PersonalBestFullInputDTO pbDto)
        {
            if (clientId != pbDto.ClientId)
                return BadRequest();

            var result = await _service.UpdatePersonalBest(pbDto);
            return result switch
            {
                CRUD_SERVICE_RESULT.BAD_REQUEST => BadRequest(),
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Update a specific client
        /// </summary>
        /// <param name="id"></param>
        /// <param name="client"></param>
        /// <response code="400">If ids in body and query do not match</response> 
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the client could not be found</response> 
        /// <response code="204">If the client was successfully updated</response>
        /// <returns></returns>
        // PUT: api/v1/Clients/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient(int id, ClientFullInputDTO client)
        {
            if (id != client.Id)
                return BadRequest();

            var result = await _service.Update(client);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();
            else if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();
            return NoContent();
        }

        /// <summary>
        /// Add a new client
        /// </summary>
        /// <param name="clientDto"></param>
        /// <returns></returns>
        /// <response code="201">If the client was successfully added</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If the client could not be found</response> 
        // POST: api/v1/Clients
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ClientFullOutputDTO>> PostClient(ClientFullInputDTO clientDto)
        {
            var result = await _service.Add(clientDto);

            if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            var client = await _service.Get(clientDto.Id);
            AddOutputURLs(client);

            return CreatedAtAction("GetClient", new { id = client.Id }, client);
        }

        /// <summary>
        /// Delete a specific client
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">If the client was successfully deleted</response> 
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/Clients/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient(int id)
        {
            await _service.Delete(id);
            return NoContent();
        }
        
        private void AddOutputURLs(ClientFullOutputDTO client)
        {
            foreach(var program in client.Programs)
            {
                program.Url = Url.Action("GetProgram", "Programs", new { Id = program.Url }, Request.Scheme);
            }
        }

        private void AddOutputURLs(PersonalBestFullOutputDTO we)
        {
            we.Exercise.Url = Url.Action("GetExercise", "Exercises", new { Id = we.Exercise.Url }, Request.Scheme);
        }
    }
}
 
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTSessionsBackend.Models;
using PTSessionsBackend.Models.DTOs;
using PTSessionsBackend.Services;

namespace PTSessionsBackend.Controllers 
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private readonly PtSessionsDbContext _context;
        private readonly SessionService _service;

        public SessionsController(PtSessionsDbContext context, SessionService service)
        {
            _service = service;
            _context = context;
        }

        /// <summary>
        /// Get all sessions
        /// </summary>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // GET: api/v1/Sessions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SessionFullOutputDTO>>> GetSession()
        {
            var sessions = await _service.GetAll();

            foreach(var sess in sessions)
                AddOutputURLs(sess);

            return Ok(sessions);
        }

        /// <summary>
        /// Get a specific session
        /// </summary>
        /// <param name="id"></param>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If session is not found</response>
        /// <returns></returns>
        // Get a specific session
        // GET: api/v1/Sessions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SessionFullOutputDTO>> GetSession(int id)
        {
            var session = await _service.Get(id);

            if (session == null)
                return NotFound();

            AddOutputURLs(session);
            return Ok(session);
        }

        /// <summary>
        /// Update an existing session
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sessionDto"></param>
        /// <response code="204">If session successfully updated</response>
        /// <response code="400">If ids in body and query do not match, or if the supplied program, venue or workout ids do not exist</response>
        /// <response code="401">If not authorized</response>
        /// <response code="404">If session does not exist, or database is interrupted</response>
        /// <returns></returns>
        // PUT: api/v1/Sessions/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSession(int id, SessionFullInputDTO sessionDto)
        {
            if (id != sessionDto.Id)
                return BadRequest();

            var result = await _service.Update(sessionDto);

            return result switch
            {
                CRUD_SERVICE_RESULT.BAD_REQUEST => BadRequest(),
                CRUD_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }

        /// <summary>
        /// Add a new session
        /// </summary>
        /// <param name="sessionDto"></param>
        /// <response code="201">If session was successfully created</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // POST: api/v1/Sessions
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SessionFullOutputDTO>> PostSession(SessionFullInputDTO sessionDto)
        {
            var result = await _service.Add(sessionDto);
            if (result == CRUD_SERVICE_RESULT.BAD_REQUEST)
                return BadRequest();
            else if (result == CRUD_SERVICE_RESULT.NOT_FOUND)
                return NotFound();

            var session = await _service.Get(sessionDto.Id);
            AddOutputURLs(session);
            return CreatedAtAction("GetSession", new { id = session.Id }, session);
        }

        /// <summary>
        /// Delete a session
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If session successfully deleted</response>
        /// <response code="401">If not authorized</response>
        /// <returns></returns>
        // DELETE: api/v1/Sessions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSession(int id)
        {
            var session = await _context.Session.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }

            _context.Session.Remove(session);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private void AddOutputURLs(SessionFullOutputDTO session)
        {
            // Pass Request.Scheme to get absolute URLs
            session.Workout.Url = Url.Action("GetWorkout", "Workouts", new { id = session.Workout.Url }, Request.Scheme);
            session.Program.Url = Url.Action("GetProgram", "Programs", new { id = session.Program.Url }, Request.Scheme);
            session.Venue.Url = Url.Action("GetVenue", "Venues", new { id = session.Venue.Url }, Request.Scheme);
        }
    }
}
